class Input(Model):
    """
    Inputs for the project
    """

    def __init__(self, cfg, input_name=None):
        super(Input, self).__init__(cfg)
        self.cfg = cfg
        self.input_name = input_name
        if len(self.inputs_list) > 1:
            if input_name is not None:
                print(input_name)
                self.name = input_name
                for inp in self.inputs_list:
                    print(inp)
                    if inp['name'] == input_name:
                        print('Fournd')
                        self.input_type = inp['type']
                        self.input_prefix = inp['type']
                        break
            else:
                print('More than one input in config. Specify input')
                raise ValueError
        else:
            print('ferk')
            self.name = cfg['model']['inputs'][0]['name']
            self.input_type = cfg['model']['inputs'][0]['type']
            self.input_prefix = cfg['model']['inputs'][0]['prefix']

        self.directory = self._get_directory()
        self.s3_url = self._get_s3_url()

        # For mro, will be second class object
        # Redeclare so objects don't get confused
        self.input_name = self.name
        self.input_directory = self.directory
        self.input_s3_url = self.s3_url
        self.s3_prefix = self._get_s3_prefix()

    def _get_input_prefix(self):
        import os
        os.listdir(self.directory)
