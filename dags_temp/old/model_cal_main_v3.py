import airflow
from airflow.models import DAG
from airflow.operators.subdag_operator import SubDagOperator
from airflow.operators.python_operator import PythonOperator

from sub_dags.ensemble_simulation import ensemble_flow_simulation

# from sub_dags.ensemble_simple import ensemble_flow_simulation

from base_model import Run, Realization

import os
import json
from datetime import datetime

#
#       Naming section
#
dag_iter = 14
dag_name = 'model_calibration_' + str(dag_iter)

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

#
#       Import config section
#
config_file = 'testing-s3fs-v2.json'
with open(config_file, 'r') as config_data:
    config = json.load(config_data)

run = Run(config)

args = {
    'owner': 'airflow',
    'start_date': datetime(2018, 4, 30)
}

main_dag = DAG(
    dag_id=dag_name,
    default_args=args,
    schedule_interval='@once'
)

max_iterations = run.max_iterations
max_realizations = run.max_realizations


def download(config):
    run = Run(config)
    if os.path.exists(run.directory):
        print('Input already exists')
    else:
        run.download()

def validate_no_existing_run(config):
    from copy import deepcopy
    cfg2 = deepcopy(config)
    cfg2['run']['input']['type'] = 'run'
    cfg2['run']['input']['name'] = cfg2['run']['run_id']
    run = Run(cfg2)
    if os.path.exists(run.directory):
        raise ValueError

def calibrate(config, i):
    import shutil
    from sys import platform
    import os
    run = Run(config)
    max_real = run.max_realizations
    for r in range(1, max_real + 1):
        old_real = Realization(config, r=r, i=i)
        print('old realization = ' + old_real.directory)
        real = Realization(config, r=r, i=i+1)
        print('new realization = ' + real.directory)
        os.system('ls ' + old_real.directory)
        shutil.copytree(old_real.directory, real.directory)
    # For common files
    if platform.lower()[:3] == 'win':
        iter_dir = '\\'.join(real.directory.split('\\')[:-1]) + '\\' + real.realization_prefix
    else:
        iter_dir = '/'.join(real.directory.split('/')[:-1])+ '/' + real.realization_prefix
    print('Copying base')
    print('From dir = ' + real.base_model_dir)
    print('To dir = ' + iter_dir)
    shutil.copytree(real.base_model_dir, iter_dir)

validate = PythonOperator(
    task_id='validate_run_output_does_not_exist_' + str(dag_iter),
    python_callable=validate_no_existing_run,
    op_args=[config],
    default_args=args,
    dag=main_dag
)

download = PythonOperator(
    task_id='download_source_files_to_efs_' + str(dag_iter),
    python_callable=download,
    op_args=[config],
    default_args=args,
    dag=main_dag
)

simulation_iterations = []
calibration_iterations = []

for i in range(1, max_iterations + 1):
    subdag_name = 'run-' + str(dag_iter) + '-simulation-iteration-' + str(i)
    simulation_iterations.append(SubDagOperator(
        task_id=subdag_name,
        subdag=ensemble_flow_simulation(dag_name, subdag_name, args, config, i),
        default_args=args,
        dag=main_dag
    ))

    subdag_name = 'run-' + str(dag_iter) + '-update-iteration-' + str(i)
    calibration_iterations.append(PythonOperator(
        task_id=subdag_name,
        python_callable=calibrate,
        op_args=[config, i],
        default_args=args,
        dag=main_dag
    ))

main_dag >> validate >> download >> simulation_iterations[0]

for i in range(0, max_iterations):

    simulation_iterations[i] >> calibration_iterations[i]
    if i != max_iterations - 1:
        calibration_iterations[i] >> simulation_iterations[i+1]





