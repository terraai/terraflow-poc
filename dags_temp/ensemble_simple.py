from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator

import os


def pre_sim_setup():
    pass


def opm_flow(r):
    os.system('flow /mnt/efs/OLYMPUS_' + str(r) + '/OLYMPUS_' + str(r) + '.DATA')


def post_sim_check():
    pass


def ensemble_flow_simulation(parent_dag_name, child_dag_name, args, max_realizations):
    ensemble_subdag = DAG(
        dag_id='%s.%s' % (parent_dag_name, child_dag_name),
        default_args=args,
        schedule_interval='@once'
    )

    for r in range(1, max_realizations + 1):
        PythonOperator(
            task_id=str(r),
            python_callable=opm_flow,
            op_args=[r],
            default_args=args,
            dag=ensemble_subdag,
            queue='cpusims'
        )

    return ensemble_subdag
