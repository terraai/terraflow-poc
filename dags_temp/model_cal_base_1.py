import airflow
from airflow.models import DAG
from airflow.operators.subdag_operator import SubDagOperator

from sub_dags.ensemble_simulation import ensemble_flow_simulation

import os
import json
from datetime import datetime

args = {
    'owner': 'airflow',
    'start_date': datetime(2018, 4, 30)
}

dag_iter = 8
dag_name = 'model_calibration_' + str(dag_iter)

main_dag = DAG(
    dag_id=dag_name,
    default_args=args,
    schedule_interval='@once'
)

# abspath = os.path.abspath(__file__)
# dname = os.path.dirname(abspath)
# os.chdir(dname)


max_iterations = 5
max_realizations = 8

simulation_iterations = []

for i in range(1, max_iterations + 1):
    subdag_name = 'run-' + str(dag_iter) + '-iteration-' + str(i)
    simulation_iterations.append(SubDagOperator(
        task_id=subdag_name,
        subdag=ensemble_flow_simulation(dag_name, subdag_name, args, max_realizations),
        default_args=args,
        dag=main_dag
    ))


    if i != 1:
        simulation_iterations[i-2].set_downstream(simulation_iterations[i-1])
