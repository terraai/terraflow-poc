import airflow
from airflow.models import DAG
from airflow.operators.subdag_operator import SubDagOperator
from airflow.operators.python_operator import PythonOperator

from sub_dags.ensemble_simulation import ensemble_flow_simulation
# from sub_dags.ensemble_simple import ensemble_flow_simulation

from base_model import Project, Model, Run, Steps

import os
import json
from datetime import datetime

#
#       Naming section
#
dag_iter = 15
dag_name = 'model_calibration_' + str(dag_iter)

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

#
#       Import config section
#
config_file = 'testing-win.json'
with open(config_file, 'r') as config_data:
    config = json.load(config_data)

prj = Project(config)
mod = Model(config)
run = Run(config)
steps = Steps(config)

args = {
    'owner': 'airflow',
    'start_date': datetime(2018, 4, 30)
}

main_dag = DAG(
    dag_id=dag_name,
    default_args=args,
    schedule_interval='@once'
)

max_iterations = 4
max_realizations = 50


def testing(prj):
    print(prj.name)

download = PythonOperator(
    task_id='download_source_files_to_efs',
    python_callable=testing,
    op_args=[prj],
    default_args=args,
    dag=main_dag
)

simulations = []
updates = []

for i in range(1, max_iterations + 1):
    sim_subdag_name = 'run-' + str(dag_iter) + '-iteration-' + str(i)
    simulations.append(SubDagOperator(
        task_id=sim_subdag_name,
        subdag=ensemble_flow_simulation(dag_name, subdag_name, args, max_realizations),
        default_args=args,
        dag=main_dag
    ))



main_dag >> download >> simulation_iterations[0]

for i in range(1, max_iterations + 1):
    if i != 1:
        simulation_iterations[i-2].set_downstream(simulation_iterations[i-1])
