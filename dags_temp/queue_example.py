import airflow
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator

import os
import json
from datetime import datetime

#
#       Naming section
#
dag_iter = 21
dag_name = 'queue_' + str(dag_iter)

args = {
    'owner': 'airflow',
    'start_date': datetime(2018, 4, 30)
}

main_dag = DAG(
    dag_id=dag_name,
    default_args=args,
    schedule_interval='@once'
)

max_iterations = 5
max_realizations = 8

simulation_iterations = []

def testing(prj):
    print(str(prj))

for i in range(1, 20):

    simulation_iterations.append(PythonOperator(
        task_id='download_source_files_to_efs_' + str(i),
        python_callable=testing,
        op_args=[i],
        default_args=args,
        queue='cpusims',
        dag=main_dag
    ))

# for i in range(1, 20):
#     if i != 1:
#         simulation_iterations[i-2].set_downstream(simulation_iterations[i-1])
