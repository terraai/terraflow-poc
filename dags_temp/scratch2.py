
all_simulators = {'opm_flow': 'flow',
                  'mrst': 'mrst',
                  'eclipse': 'eclipse',
                  'echelon': 'echelon'}


print(all_simulators['opm_flow'])
# some_txt = 'Blah Blah Blah Blah Blah Blah Blah '
# mpirun -np 8 flow /mnt/efs/OLYMPUS_13/OLYMPUS_13.DATA

import subprocess
proc = subprocess.Popen(['flow /mnt/efs/OLYMPUS_2/OLYMPUS_2.DATA'], stdout=subprocess.PIPE, shell=True)
(out, err) = proc.communicate()
with open('log.txt', 'w') as log_output:
    log_output.write(out)