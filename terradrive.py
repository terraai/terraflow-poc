from datetime import datetime

from airflow.utils.db import provide_session
from airflow import settings
from airflow.utils.log.logging_mixin import LoggingMixin

log = LoggingMixin().log

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, create_engine, MetaData
from sqlalchemy.orm import sessionmaker, relationship

schema = 'terraflow'

meta = MetaData(schema=schema)

Base = declarative_base()


def add_new_connection():
    """
    Create a new connection in the connections table in the rd backend for
    """
    pass


def init_terra_db():
    """
    Adds new tables so that metadata on the file store can be saved
    """
    pass


def populate_project_directory_metastore(base_dir, project):
    """
    Create entry in project directory for new files and folders and initialize them in the metastore
    """
    pass


class Directory(object):

    def _level_constructor(self):
        levels = ['project', 'model', 'runs']
        level = {'projects':
                     {'models':{
                         'runs'
                     }}}

    # @provide_session
    def download(self):
        pass

    # @provide_session
    def get_directory(self):
        pass

    # @provide_session
    def make_directory(self):
        pass

    # @provide_session
    def delete_directory(self):
        pass

    def sync_directory(self):
        pass

class DataStores(Base):
    __tablename__ = 'datastores'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    creation_date = datetime.now()


class Project(Base, Directory):
    __tablename__ = 'projects'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    creation_date = datetime.now()
    primary_datastore = Column(String())



class Model(Base, Directory):
    __tablename__ = 'models'
    id = Column(Integer, primary_key=True)
    model_name = Column(String)
    project_id = Column(Integer(), ForeignKey('project.id'))
    project = Column(Integer())


class Run(Base, Directory):
    __tablename__ = 'run'
    id = Column(Integer, primary_key=True)
    run_id = Column(String)
    run_date = datetime.now()


class Sources(Base):
    __tablename__ = 'sources'
    id = Column(Integer, primary_key=True)
    run_id = Column(String)


class SimulationModel(Base):
    __tablename__ = 'simulation'
    id = Column(Integer, primary_key=True)
    run_id = Column(String)

class FlowSimulators(Base):
    __tablename__ = 'flow_simulators'
    id = Column(Integer, primary_key=True)
    name = Column(String)

if __name__ == '__main__':
    # url = 'postgres+psycopg2://terra:fundus500k@fetl-ds.c4lstuf6msdr.us-west-2.rds.amazonaws.com:5432/metl'
    # engine = create_engine(url, echo=True)
    # Base.metadata.create_all(engine)
    # sess = sessionmaker(engine, expire_on_commit=False)()
    #
    # prj = Project(name='sdvx')
    # mod = Model(model_name='duh')
    #
    # sess.add(prj)
    # sess.add(mod)
    # sess.commit()
    # sess.close()

    url = 'postgres+psycopg2://terra:fundus500k@fetl-ds.c4lstuf6msdr.us-west-2.rds.amazonaws.com:5432/metl'
    engine = create_engine(url, echo=True)
    Base.metadata.create_all(engine)
    sess = sessionmaker(engine, expire_on_commit=False)()
    prj = Project(name='what???', primary_datastore='s3')
    sess.add(prj)
    sess.commit()
    sess.close()