from airflow.hooks.base_hook import BaseHook


class TerraDriveBaseHook(BaseHook):
    import os
    def __init__(self,
                 level=None,
                 iteration=None,
                 realization=None,
                 base_dir=None):
        self.level = level
        self.iteration = iteration
        self.realization = realization
        self.base_dir = base_dir

    def get_conn(self):
        """

        """
        pass

    def get_dir(self):
        pass

    def get_source(self):
        pass

    def make_dir(self):
        pass

    def make_run(self):
        pass

