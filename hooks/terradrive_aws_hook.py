from airflow.exceptions import AirflowException
from airflow.contrib.hooks.aws_hook import AwsHook
from airflow.contrib.hooks.aws_hook import _parse_s3_config

access_key, secret_key = _parse_s3_config


class S3Hook(AwsHook):
    pass
