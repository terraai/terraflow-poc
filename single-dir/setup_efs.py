

import boto3



intialize_dags() {
    echo "Initializing dags"
    if [ ! -d "/mnt/efs/dags" ]; then
        echo "Copying dags folder and sym linking to $AIRFLOW_HOME"
        aws s3 cp s3://terraflow/dags.zip /mnt/efs/dags.zip
        unzip /mnt/efs/dags.zip -d /mnt/efs/
    else
#        TODO This is where we put an overide - Figure out workflow first
        echo "Dags directory already exists"
    fi
    if [ ! -d "$AIRFLOW_HOME/dags" ]; then
        echo "Symlinking dags"
        ln -s /mnt/efs/dags $AIRFLOW_HOME
    else
        echo "Dags folder in airflow home already extsts"
    fi
    echo "Copying dag_helpers, sims, updates, airflow_helpers folders to efs and sym linking them to dags folder"
    declare -a folders=("dag_helpers" "sims" "updates" "airflow_helpers")
    for i in "${folders[@]}"
        do
        if [ ! -d "/mnt/efs/$i" ]; then
            aws s3 cp s3://terraflow/"$i".zip /mnt/efs/"$i".zip
            unzip /mnt/efs/"$i".zip -d /mnt/efs/
        else
#        TODO This is where we put an overide - Figure out workflow first
            echo " directory already exists"
        fi
        if [ ! -d "$AIRFLOW_HOME/dags/$i" ]; then
            ln -s /mnt/efs/"$i" $AIRFLOW_HOME/dags/
        else
            echo "$i already exists in $AIRFLOW_HOME/dags/"
        fi
    done
}


