import os
from airflow.models import DAG
from airflow.operators import PythonOperator

def flow_sim_exec(cfg, model_dir, model_name, mpi_settings = None):
    """
    :param cfg:
    :param model_dir:
    :param sim_settings:
    :param mpi_settings:
    :return:
    """
    print("Using " + cfg.simulator.name + " " + cfg.simulator.type + "simulator")
    # TODO Move to unit test on cfg
    all_simulators = ['opm_flow', 'mrst', 'eclipse', 'echelon']
    if cfg.simulator.name not in all_simulators:
        error_msg = "{} not in list of acceptable simulators {}".format(cfg.simulator.name, str(all_simulators).strip('[]'))
        raise ValueError(error_msg)

    if cfg.simulator.name == 'opm_flow':
        """
        OPM simulator 
        """
        if cfg.simulator.settings:
            settings_string = flatten_settings(cfg)
        else:
            settings_string = ""

        model_path = model_dir + model_name + '.DATA'

        cmd_list = ['flow']
        cmd_list.extend((settings_string, model_path))
        bash_cmd = " ".join(cmd_list)
        print(bash_cmd)
        # os.system(bash_cmd)

    if cfg.simulator.name == 'mrst':
        """
        MRST 
        """
        if cfg.simulator.settings:
            settings_string = flatten_settings(cfg)
        else:
            settings_string = ""

        model_path = model_dir + model_name + '.DATA'

        cmd_list = ['mrst']
        cmd_list.extend((settings_string, model_path))
        bash_cmd = " ".join(cmd_list)
        print(bash_cmd)
        os.system(bash_cmd)

    if cfg.simulator.name == 'eclipse':
        """
        SLB's Eclipse 
        """
        if cfg.simulator.settings:
            settings_string = flatten_settings(cfg)
        else:
            settings_string = ""

        model_path = model_dir + model_name + '.DATA'

        cmd_list = ['eclipse']
        cmd_list.extend((settings_string, model_path))
        bash_cmd = " ".join(cmd_list)
        print(bash_cmd)
        os.system(bash_cmd)

    if cfg.simulator.name == 'echelon':
        """
        Stone Ridge Echelon
        """
        if cfg.simulator.settings:
            settings_string = flatten_settings(cfg)
        else:
            settings_string = ""

        model_path = model_dir + model_name + '.DATA'

        cmd_list = ['echelon']
        cmd_list.extend((settings_string, model_path))
        bash_cmd = " ".join(cmd_list)
        print(bash_cmd)
        os.system(bash_cmd)

def flatten_settings(cfg):
    sim_settings = ""
    for s in range(len(cfg.simulator.settings)):
        sim_settings = sim_settings + " " + list(cfg.simulator.settings[s]._asdict().items())[0][0] + "=" + \
            list(cfg.simulator.settings[s]._asdict().items())[0][1]
    return sim_settings

# Dag is returned by a factory method
def sub_dag(parent_dag_name,
            child_dag_name,
            args,
            schedule_interval,
            max_realizations,
            current_iteration=1,
            current_realization=1):

  dag = DAG(
    '%s.%s' % (parent_dag_name, child_dag_name),
    default_args=args,
    start_date=args['start_date'],
    max_active_runs=1,
  )

  for r in range(1, cfg.realizations['max'] + 1):
    # Realization loop
    if r > current_realization or i > cfg.iteration.current:
      PythonOperator(
        task_id='sim-task-' + str(r) + '-agg-task-' + str(i),
        python_callable=sim_call(cfg, i, r),
        # params=params,
        # queue=queue,
        dag=dag
      )

  return dag