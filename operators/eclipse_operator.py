from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults


class EclipseOperator(BaseOperator):

    @apply_defaults
    def __init__(
            self,
            source_deck,
            settings=None,
            license_server=None,
            *args, **kwargs):
        super(EclipseOperator, self).__init__(*args, **kwargs)
        self.source_deck = source_deck
        self.settings = settings
        self.license_server = license_server

    def flatten_settings(self):
        pass

    def test_license_sever(self):
        pass

    def test_inputs(self):
        pass

    def execute(self, context):
        pass
