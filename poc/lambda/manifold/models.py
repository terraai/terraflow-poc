from sqlalchemy import create_engine, Column, Integer, String, ForeignKey
from sqlalchemy.orm import Session, relationship, backref
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Employee(Base):
    __tablename__ = 'personnel'
    __mapper_args__ = {'polymorphic_on': 'etyp'}

    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    etyp = Column(String(10))

class Engineer(Employee):
    __tablename__ = "engineers"
    __mapper_args__ = {'polymorphic_identity': 'eng'}
    id = Column(Integer, ForeignKey(Employee.id), primary_key=True)
    eng_data = Column(String(50))


url = 'postgres+psycopg2://terraflow:terraflow@terraflow.ccdtpkr2drkq.us-east-2.rds.amazonaws.com:5432/terraflow'

engine = create_engine(url, echo=True)
Base.metadata.create_all(engine)
session = Session(bind=engine)

e1 = Engineer(name="wally", eng_data="lazy guy")

session.add(e1)
session.commit()
# note that the Engineer table is not INSERTed into

e1 = session.query(Employee).filter_by(name="wally").one()
# the next line triggers an exception because it tries to lookup
# the Engineer row, which is missing
print(e1.eng_data)