import psycopg2
from sshtunnel import SSHTunnelForwarder

import os

# For interactive work (on ipython) it's easier to work with explicit objects
# instead of contexts.

# Create an SSH tunnel
# tunnel = SSHTunnelForwarder(
#     ('18.216.169.46', 22),
#     ssh_username='<username>',
#     ssh_private_key='cellspace-root',
#     remote_bind_address=('localhost', 5432),
#     local_bind_address=('localhost',6543), # could be any available port
# )

# tunnel.start()

dbname = 'terraflow'
user = 'terraflow'
host = 'terraflow.ccdtpkr2drkq.us-east-2.rds.amazonaws.com'
password = 'terraflow'

key_name = 'stuff'

# Start the tunnel
with SSHTunnelForwarder(('18.216.169.46', 22),
                        ssh_username='ec2-user',
                        ssh_private_key='cellspace-root',
                        remote_bind_address=(host, 5432),
                        local_bind_address=('localhost', 6543)) as tunnel:

    with psycopg2.connect(database=dbname,
                          user=user,
                          host=tunnel.local_bind_host,
                          port=tunnel.local_bind_port,
                          password=password) as conn:

        cur = conn.cursor()
        cur.execute("INSERT INTO models_test (model_name) VALUES ('%s')" % key_name)
        conn.commit()
        cur.close()