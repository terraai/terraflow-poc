from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (MetaData, Table, Column, Integer, String, ForeignKey, Date, Boolean,
                        create_engine)
from sqlalchemy.orm import (mapper, relationship, sessionmaker)
from sqlalchemy.orm import polymorphic_union

from functools import wraps

Base = declarative_base()

class Testing(Base):

    __tablename__ = 'testing'
    id = Column(Integer, primary_key=True)
    name = Column(String)

class Model(Base):
    __tablename__ = 'models_test'
    id = Column(Integer, primary_key=True)
    # project_id = (Integer, ForeignKey('testing.id'))
    model_name = Column(String)


if __name__ == '__main__':

    url = 'postgres+psycopg2://terraflow:terraflow@terraflow.ccdtpkr2drkq.us-east-2.rds.amazonaws.com:5432/terraflow'

    engine = create_engine(url, echo=True)
    schema = 'terraflow'
    # meta = MetaData(schema=schema, bind=engine)
    # meta.create_all(engine)
    Base.metadata.create_all(engine)

    sess = sessionmaker(engine, expire_on_commit=False)()

    tst = Model(
        model_name='err_things'
    )

    sess.add(tst)
    sess.commit()
    sess.close()