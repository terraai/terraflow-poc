from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (Column, Integer, String, DateTime,
                        create_engine, ForeignKey)
from sqlalchemy.orm import (sessionmaker, relationship)
import json
import urllib.parse
import boto3
from datetime import datetime

from sshtunnel import SSHTunnelForwarder

print('Loading function')

s3 = boto3.client('s3')

Base = declarative_base()

class Project(Base):
    __tablename__ = 'projects'
    id = Column(Integer, primary_key=True)
    project_name = Column(String)

    models = relationship("Model", backref="projects")


class Model(Base):
    __tablename__ = 'models'
    id = Column(Integer, primary_key=True)
    project_id = Column(Integer, ForeignKey("projects.id"))
    model_name = Column(String)
    last_update = Column(DateTime)


def lambda_handler(event, context):
    print("Received event: " + json.dumps(event, indent=2))

    # Get the object from the event and show its content type
    bucket = event['Records'][0]['s3']['bucket']['name']
    key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')

    host = 'terraflow.ccdtpkr2drkq.us-east-2.rds.amazonaws.com'

    with SSHTunnelForwarder(('18.216.169.46', 22),
                            ssh_username='ec2-user',
                            ssh_private_key='cellspace-root',
                            remote_bind_address=(host, 5432),
                            local_bind_address=('localhost', 6543)) as tunnel:

        try:
            response = s3.get_object(Bucket=bucket, Key=key)
            print("CONTENT TYPE: " + response['ContentType'])
            print("KEY: " + key)

            remote_host = str(tunnel.local_bind_host)
            local_port = str(tunnel.local_bind_port)

            conn_str = 'postgresql://terraflow:terraflow@{}:{}/terraflow'.format(remote_host, local_port)
            engine = create_engine(conn_str, echo=True)

            Base.metadata.create_all(engine)

            sess = sessionmaker(engine, expire_on_commit=False)()

            proj = Project(project_name='testing2')
            sess.merge(proj)
            sess.commit()

            # query = sess.query(Project).all()
            # print(query)
            # print('here')


            # sess.add(proj)

            sess.flush()

            mod = Model(model_name=key, project_id=proj.id, last_update=datetime.now())

            sess.add(mod)
            sess.commit()
            sess.close()

            return response['ContentType']
        except Exception as e:
            print(e)
            print('Error getting object {} from bucket {}. Make sure they exist and your bucket is in the same region as this function.'.format(key, bucket))
            raise e



if __name__ == '__main__':

    with open('event.json') as input_file:
        event = json.load(input_file)

    lambda_handler(event=event, context=None)
