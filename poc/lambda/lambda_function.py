from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (Column, Integer, String,
                        create_engine)
from sqlalchemy.orm import (sessionmaker)
import json
import urllib.parse
import boto3

print('Loading function')

s3 = boto3.client('s3')

Base = declarative_base()

class Model(Base):
    __tablename__ = 'models_test'
    id = Column(Integer, primary_key=True)
    model_name = Column(String)

def lambda_handler(event, context):
    print("Received event: " + json.dumps(event, indent=2))

    # Get the object from the event and show its content type
    bucket = event['Records'][0]['s3']['bucket']['name']
    key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')
    try:
        response = s3.get_object(Bucket=bucket, Key=key)
        print("CONTENT TYPE: " + response['ContentType'])
        print("KEY: " + key)

        url = 'postgres+psycopg2://terraflow:terraflow@terraflow.ccdtpkr2drkq.us-east-2.rds.amazonaws.com:5432/terraflow'
        engine = create_engine(url, echo=True)
        Base.metadata.create_all(engine)

        sess = sessionmaker(engine, expire_on_commit=False)()

        mod = Model(model_name=key)

        sess.add(mod)
        sess.commit()
        sess.close()

        return response['ContentType']
    except Exception as e:
        print(e)
        print('Error getting object {} from bucket {}. Make sure they exist and your bucket is in the same region as this function.'.format(key, bucket))
        raise e



