#!/usr/bin/env bash
cd packages
zip -r9 lambda_function.zip *
cd ..
zip -g lambda_function.zip lambda_function.py
#aws s3 cp TestingSQL.zip s3://terraflow-lambda/s3-index/TestingSQL.zip
#aws s3 cp s3://terraflow-lambda/s3-index/TestingSQL.zip TestingSQL.zip