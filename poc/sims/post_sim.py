# Copyright (C) terra.ai Inc. - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Reza Khaninezhad and Rob Cannon and <rob@terra.ai> 2018

import os

def check_sim_run(cfg, model_dir):
    # This function checks that we have at least one file with the extension listed in the particular simulator
    # file extension list

    print("Checking the ouput of the sims in " + model_dir)
    output_extensions = []
    valid_sim = False

    # TODO Do we need to validate more than the extensions? Right now we just check to see if these exist
    validation_counter = 0

    flow_simulator = cfg.simulator.name
    # List of valid simulation extensions
    if flow_simulator == 'opm_flow':
        output_extensions = ['rsm']

    if flow_simulator == 'mrst':
        output_extensions = ['rsm']

    if flow_simulator == 'eclipse':
        output_extensions = ['rsm']

    if flow_simulator == 'echelon':
        output_extensions = ['GRD']

    # Get files in model directory
    if os.path.exists(model_dir):
        files = os.listdir(model_dir)
        for f in range(0, len(files)):
            for e in range(0, len(output_extensions)):
                file_extension = os.path.splitext(model_dir + files[f])[1][1:]

                if output_extensions[e] == file_extension:
                    validation_counter = validation_counter + 1

        if validation_counter >= len(output_extensions):
            valid_sim = True
    else:
        print("%s doesn't exist yet" % model_dir)

    return valid_sim

# flag = check_sim_run('echelon', "/Users/robcannon/Documents/Olympus/Eclipse/OLYMPUS_2")
# if flag:
#     print "Yes"
# else:
#     print "no"

def move_sim_outputs(model_dir, model_name):
    print("Moving " + model_name + " from " + model_dir)
    return True


