# Copyright (C) terra.ai Inc. - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Reza Khaninezhad and Rob Cannon and <rob@terra.ai> 2018

from dag_helpers.directories import model_directory
from dag_helpers.namers import model_namer
from sims.flow_sim import flow_sim_exec
from sims.post_sim import check_sim_run


def sim_call(cfg, i, r):
    """
    :param cfg:
    :param i:
    :param r:
    :return:
    """
    model_dir = model_directory(cfg, i, r)
    model_name = model_namer(cfg, i, r)

    # Execute flow sim
    print("Running simulation in directory " + model_dir + " and name = " + model_name)
    flow_sim_exec(cfg, model_dir, model_name)

    # Check simulation
    if check_sim_run(cfg, model_dir):
        print("Successful run of " + model_name)
    else:
        print("The simulation " + model_name + " was not successful")



