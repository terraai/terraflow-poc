# Copyright (C) terra.ai Inc. - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Reza Khaninezhad and Rob Cannon and <rob@terra.ai> 2018

from random import random
import time
import os

def testing_agg(dir, agg_name="testing.txt", sim_size=1024):

    if not os.path.isdir(dir):
        print('Making dir')
        os.mkdir(dir)

    wait = random()

    time.sleep(wait)
    print("waited " + str(wait))

    # Write test file
    try:
        with open(dir + agg_name, "wb") as out:
            out.truncate(sim_size)
    except:
        raise TypeError
