
provider "aws" {
  region = "us-east-2"
  access_key = "{{ AWS_ACCESS_KEY_ID }}"
  secret_key = "{{ AWS_SECRET_ACCESS_KEY }}"
}

terraform {
  backend "s3" {
    bucket = "terranet-states"
    key    = "prod/vpc/terraform.tfstate"
    region = "us-east-2"
  }
}


resource "" "" {}