
from jinja2 import Environment, FileSystemLoader
import os

# Capture our current directory
THIS_DIR = os.path.dirname(os.path.abspath(__file__))

def print_html_doc():
    # Create the jinja2 environment.
    # Notice the use of trim_blocks, which greatly helps control whitespace.
    j2_env = Environment(loader=FileSystemLoader(THIS_DIR),
                         trim_blocks=True)

    print(j2_env.get_template('main.tf').render(
        AWS_ACCESS_KEY_ID='XXX',
        AWS_SECRET_ACCESS_KEY='YYY'
    ))

if __name__ == '__main__':
    print_html_doc()
