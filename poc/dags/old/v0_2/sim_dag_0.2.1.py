# Copyright (C) terra.ai Inc. - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Reza Khaninezhad and Rob Cannon and <rob@terra.ai> 2018

# General Imports
import os

# Airflow imports
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator

# Custom modules
from dags.models import Project, Models, Runs, Steps


# Replace this when automating construction
config_path = '%CONFIG_PATH%'
print(os.path.dirname(os.path.realpath(__file__)) + '/testing-win.json')

if config_path[:1] == '%':
    config_path = os.path.dirname(os.path.realpath(__file__)) + '/mac-testing-win.json'

prj = Project(config_path)
mod = Models(config_path)
run = Runs(config_path)
steps = Steps(config_path)

print(run)

args = {
    'owner': 'airflow',
    'start_date': run.start_date,
}

dag = DAG(
    dag_id=run.run_id,
    default_args=args
)

run.setup_steps = []
for step in range(0, steps.num_setup_steps):

    # import_statement = steps.build_import_string('setup', step)
    # exec(import_statement)
    # import types
    from setup_steps.setup_steps import scale_cpu_cluster

    # function_statement = steps.build_function_string('setup', step)
    # exec(function_statement)
    # print(callable(eval(function_statement)))
    # fun = locals()[steps.setup_steps[0]['function']]
    # print(fun)
    # fun(number_nodes=3, node_type="c5.xlarge")
    # argst = steps.setup_steps[0]['args']
    # print(argst)
    #
    # argst = {'number_nodes': 3, 'node_type': 'c5.xlarge'}
    # fun(**argst)
    # print('calling')
    #
    # print(type(fun))
    # print(type(fun(**argst)))
    # # dynf = types.FunctionType(fun, argst)

    scale_cpu_cluster(number_nodes=3, node_type="c5.xlarge")

    # if callable(fun(**argst)):
    #     print('calling')
    # print(callable(fun))
    run.setup_steps.append(PythonOperator(
        task_id='-'.join(['setup-step', steps.setup_steps[step]['name']]),
        # python_callable=locals()[steps.setup_steps[step]['function']],
        python_callable=scale_cpu_cluster(number_nodes=step, node_type="c5.xlarge"),
        # params=params,
        # queue=queue,
        dag=dag
    ))
    # Schedule in loop
    if step != 0:
        run.setup_steps[step] >> run.setup_steps[step - 1]

run.sim_tasks = []
for step in range(steps.num_sim_steps):
    # Step loop
    import_statement = steps.build_import_string('sim', step)
    exec(import_statement)

    temp_iter_tasks = []
    for i in range(1, run.max_iterations + 1):
        # Aggregation loop
        temp_sim_tasks = []
        if i >= run.current_iteration:
            # Temporary list of sims

            for r in range(1, run.max_realizations + 1):
                # Realization loop
                if r > run.current_realization or i > run.current_iteration:
                    function_statement = steps.build_function_string('sim', step)

                    temp_sim_tasks.append(PythonOperator(
                        task_id='sim-task-' + str(r) + '-agg-task-' + str(i),
                        python_callable=exec(function_statement),
                        # params=params,
                        # queue=queue,
                        dag=dag
                    ))
            # List of lists stored for later reference
            temp_iter_tasks.append(temp_sim_tasks)
    run.sim_tasks.append(temp_iter_tasks)


for step in range(steps.num_update_steps):
    for i in range(steps.num_update_steps):
        # Single list for all agg tasks
        if i != run.max_iterations:
            update_tasks.append(PythonOperator(
                task_id='agg-task-' + str(i),
                python_callable=update_call(cfg, i),
                # params='',
                dag=dag
            ))

for i in range(0, len(run.update_tasks)):
    for r in range(0, len(run.sim_tasks[i])):
        run.sim_tasks[i][r].set_downstream(run.update_tasks[i])

for i in range(0, len(run.update_tasks)):
    for r in range(0, cfg.realizations.max):
        run.update_tasks[i].set_downstream(run.sim_tasks[i+1][r])

