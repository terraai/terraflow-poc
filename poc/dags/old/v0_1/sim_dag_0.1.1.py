# Copyright (C) terra.ai Inc. - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Reza Khaninezhad and Rob Cannon and <rob@terra.ai> 2018

# General Imports
import os

# Airflow imports
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator

# Custom modules
from dags.models import Runs
from sims.sim_call import sim_call
from updates.update_call import update_call
# Replace this when automating construction
config_path = '%CONFIG_PATH%'
print(os.path.dirname(os.path.realpath(__file__)) + '/testing-win.json')

if config_path[:1] == '%':
    config_path = os.path.dirname(os.path.realpath(__file__)) + '/mac-testing-win.json'

run = Runs(config_path)

args = {
    'owner': 'airflow',
    'start_date':run.start_date,
}


# 'schedule_interval':run.schedule_interval

dag = DAG(
    dag_id=run.run_id,
    default_args=args
)

# The order goes realization r then agg i
# Schedule during creation
# i for iterations
# r for realizations

sim_tasks = []
update_tasks = []


for i in range(1, run.max_iterations + 1):
    """
    Build list of lists storing task objects to be scheduled in a later loop.
    """
    # Aggregation loop
    if i >= run.current_iteration:
        temp_sim_tasks = []
        # Temporary list of sims
        for r in range(1, run.max_realizations + 1):
            # Realization loop
            if r > run.current_realization or i > run.max_iterations:
                temp_sim_tasks.append(PythonOperator(
                    task_id='sim-task-' + str(r) + '-agg-task-' + str(i),
                    python_callable=sim_call(run, i, r),
                    # params=params,
                    # queue=queue,
                    dag=dag
                ))

        # List of lists stored for later reference
        sim_tasks.append(temp_sim_tasks)
        # Single list for all agg tasks
        if i != run.max_realizations:
            update_tasks.append(PythonOperator(
                task_id='agg-task-' + str(i),
                python_callable=update_call(run, i),
                # params='',
                dag=dag
            ))

for i in range(0, len(update_tasks)):
    for r in range(0, len(sim_tasks[i])):
        sim_tasks[i][r].set_downstream(update_tasks[i])

for i in range(0, len(update_tasks)):
    for r in range(0, run.max_realizations):
        update_tasks[i].set_downstream(sim_tasks[i+1][r])

