# Copyright (C) terra.ai Inc. - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Reza Khaninezhad and Rob Cannon and <rob@terra.ai> 2018

# General Imports
import os

# Airflow imports
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator

# Custom modules
from dags.models import Project, Models, Runs, Steps


# Replace this when automating construction
config_path = '%CONFIG_PATH%'
print(os.path.dirname(os.path.realpath(__file__)) + '/testing-win.json')

if config_path[:1] == '%':
    config_path = os.path.dirname(os.path.realpath(__file__)) + '/mac-testing-win.json'

prj = Project(config_path)
mod = Models(config_path)
run = Runs(config_path)
steps = Steps(config_path)

print(run)

args = {
    'owner': 'airflow',
    'start_date': run.start_date,
}

dag = DAG(
    dag_id=run.run_id,
    default_args=args
)

run.setup_steps = []
for step in range(0, steps.num_setup_steps):

    import_statement = steps.build_import_string('setup', step)
    exec(import_statement)

    function_statement = steps.build_function_string('setup', step)

    run.setup_steps.append(PythonOperator(
        task_id='-'.join(['setup-step', steps.setup_steps[step]['name']]),
        python_callable=exec(function_statement),
        # params=params,
        # queue=queue,
        dag=dag
    ))
    # Schedule in loop
    if step != 0:
        run.setup_steps[step] >> run.setup_steps[step - 1]

run.sim_steps = []
run.sim_tasks = []
for step in range(0, steps.num_sim_steps):
    import_statement = steps.build_import_string('sim', step)
    exec(import_statement)

    function_statement = steps.build_function_string('sim', step)

    for i in range(1, run.max_iterations + 1):
        """
        Build list of lists storing task objects to be scheduled in a later loop.
        """
        # Aggregation loop
        if i >= run.current_iteration:
            temp_sim_tasks = []
            # Temporary list of sims
            for r in range(1, run.max_realizations + 1):
                # Realization loop
                if r > run.current_realization or i > run.current_iteration:
                    tmp_function_statement = function_statement.replace({i}, )
                    temp_sim_tasks.append(PythonOperator(
                        task_id='sim-task-' + str(r) + '-agg-task-' + str(i),
                        python_callable=exec(function_statement.replace()),
                        # params=params,
                        # queue=queue,
                        dag=dag
                    ))
        # List of lists stored for later reference
        run.sim_tasks.append(temp_sim_tasks)

for step in range(steps.num_update_steps):
    for i in range(steps.num_update_steps):
        # Single list for all agg tasks
        if i != run.max_iterations:
            update_tasks.append(PythonOperator(
                task_id='agg-task-' + str(i),
                python_callable=update_call(cfg, i),
                # params='',
                dag=dag
            ))

for i in range(0, len(run.update_tasks)):
    for r in range(0, len(run.sim_tasks[i])):
        run.sim_tasks[i][r].set_downstream(run.update_tasks[i])

for i in range(0, len(run.update_tasks)):
    for r in range(0, cfg.realizations.max):
        run.update_tasks[i].set_downstream(run.sim_tasks[i+1][r])

