

class Project(object):
    """

    """

    def __init__(self, cfg_path):
        import json
        # from collections import namedtuple
        with open(cfg_path) as json_data:
            config = json.load(json_data)
            # self.cfg = json.load(json_data, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
        self.name = config['project']['name']
        self.directory = config['project']['directory']
        self.project_directory = '/'.join([self.directory, self.name])
        self.bucket = 'terraai-projects'

    def initialize_bucket(self, bucket='terraai-projects'):
        import boto3
        client = boto3.client('s3')

        client.put_object(
            Body='',
            Bucket=bucket,
            Key=self.name + '/'
        )

    def import_from_s3(self,
                       destination = '.',
                       bucket='terraai-projects'):
        import os
        os.system(' '.join(['aws s3 cp s3://'+'/'.join([bucket, self.name]), destination, '--recursive']))


    def upload_to_s3(self,
                     project_dir=None,
                     bucket='terraai-projects',
                     zip=False
                     ):
        import os

        if not project_dir:
            project_dir = '/'.join([self.directory, self.name])

        print(project_dir)
        if os.path.exists(project_dir):
            models = os.listdir(project_dir)
            models = list(filter(lambda a: a[:1] != '.', models))
            if zip:
                print('Implement this ')
            else:
                for m in models:
                    print(' '.join(['aws s3 cp ', '/'.join([project_dir, m]),
                                    's3://' + '/'.join([bucket, self.name]), '--recursive']))


class Models(object):

    def __init__(self, cfg_path):
        import json
        import os
        # from collections import namedtuple
        with open (cfg_path) as json_data:
            config = json.load(json_data)
            # self.cfg = json.load(json_data, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))

        self.name = config['model']['name']
        self.project_name = config['project']['name']
        self.project_directory = config['project']['directory']

        if config['run']['iterations']['current'] != 1:
            self.model_sub_directory = '/'.join(['Runs', config['run']['id']])
        else:
            self.model_sub_directory = '/'.join(['Sources', config['model']['input']])

        self.local_dir = '/'.join([
            self.project_directory,
            self.project_name,
            self.name,
            self.model_sub_directory])

        self.s3_source_key = '/'.join([self.project_name,
                                       self.name,
                                       self.model_sub_directory])

        self.model_list = list(filter(lambda a: a[:1] != '.', os.listdir(self.local_dir)))
        self.common_model_name = list(filter(lambda a: len(a.split('_')) != 2, self.model_list))[0]
        self.common_model_local_dir = '/'.join([self.local_dir,
                                                self.common_model_name])
        self.common_model_s3_key = '/'.join([self.s3_source_key, self.common_model_name])

        self.prefix = config['model']['prefix']

        self.count = len(self.model_list) - 1
        self.current_iteration = config['run']['iterations']['current']
        self.max_iterations = config['run']['iterations']['max']
        self.current_realization = config['run']['realizations']['current']
        self.max_realizations = config['run']['realizations']['max']
        self.current = '_'.join([self.prefix, str(self.current_realization), str(self.current_iteration)])
        self.destination = '.'
        self.bucket = 'terraai-projects'

    def __iter__(self):
        return self

    def next_realization(self):  # Python 3: def __next__(self)
        if self.current_realization <= self.max_realizations:
            raise StopIteration
        else:
            self.current_realization = '_'.join([self.prefix, self.current_realization + 1, self.current_iteration])

    def next_iteration(self):
        if self.current_realization <= self.max_realizations:
            raise StopIteration
        else:
            self.current_realization = '_'.join([self.prefix, self.current_realization, self.current_iteration + 1])

    def init_s3_model(self, bucket='terraai-projects'):
        import boto3
        import botocore

        # Create project subfolder
        client = boto3.client('s3')
        try:
            client.put_object(
                Body='',
                Bucket=bucket,
                Key='/'.join([self.project_name, self.name, 'Sources/'])
            )
            client.put_object(
                Body='',
                Bucket=bucket,
                Key='/'.join([self.project_name, self.name, 'Runs/'])
            )
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                print("The object does not exist.")
            else:
                raise

    def export_to_s3(self,
                     bucket='terraai-projects'):
        import os
        print(' '.join(['aws s3 cp',
                        self.local_dir,
                        's3://' + '/'.join([bucket, self.s3_source_key]),
                        '--recursive']))


    def import_from_s3(self,
                       destination = '.',
                       bucket='terraai-projects'):
        import os
        print(' '.join(['aws s3 cp',
                        's3://'+'/'.join([bucket, self.s3_source_key]),
                        destination,
                        '--recursive']))

    def run_model_ensemble(self):
        print('Running latest ensemble')

    def run_single_model(self,
                         model_name
                         ):
        print('Running single model \nname = %s' % model_name)


class Runs(object):
    def __init__(self, cfg_path):
        import json
        import os
        # from collections import namedtuple
        with open(cfg_path) as json_data:
            config = json.load(json_data)
            # self.cfg = json.load(json_data, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))

        self.run_id = config['run']['id']
        self.schedule_interval = config['run']['schedule_interval']
        self.start_date = config['run']['start_date']

        self.model_name = config['model']['name']
        self.project_name = config['project']['name']
        self.project_dir = config['project']['directory']

        if config['run']['iterations']['current'] != 1:
            self.model_sub_dir = '/'.join(['Runs', config['run']['id']])
        else:
            self.model_sub_dir = '/'.join(['Sources', config['model']['input']])

        self.run_dir = '/'.join([
            self.project_dir,
            self.project_name,
            self.model_name,
            self.model_sub_dir,
            self.run_id
        ])

        self.current_iteration = config['run']['iterations']['current']
        self.max_iterations = config['run']['iterations']['max']
        self.current_realization = config['run']['realizations']['current']
        self.max_realizations = config['run']['realizations']['max']

        if os.path.exists(self.run_dir):
            self.run_list = list(filter(lambda a: a[:1] != '.', os.listdir(self.run_dir)))
        else:
            self.run_list = []

        self.prefix = config['model']['prefix']

        self.count = len(self.run_list)

        self.bucket = 'terraai-projects'

    def compress_entire_run(self):
        print('Compressing entire run \nrun_id = %s' % self.run_id)

    def upload_entire_run_to_s3(self):
        print('Uploading entire run to s3\nrun_id = %s\nbucket = %s\nproject = %s'
              % (self.run_id, self.bucket, self.project_name))


class Steps(object):

    def __init__(self, cfg_path):
        import json
        with open(cfg_path) as json_data:
            config = json.load(json_data)

        self.steps = config['steps']
        self.stage_names = ['setup', 'sim', 'update', 'finalize']

        self.num_setup_steps = len(config['steps']['setup'])
        self.setup_steps = config['steps']['setup']

        self.num_sim_steps = len(config['steps']['sim'])
        self.sim_steps = config['steps']['sim']

        self.num_update_steps = len(config['steps']['update'])
        self.update_steps = config['steps']['update']

        self.num_finalize_steps = len(config['steps']['finalize'])
        self.finalize_steps = config['steps']['finalize']

        self.current_stage_num = 0
        self.current_stage = 'setup'
        self.current_stage_name = config['steps']['setup'][0]['name']

        self.current_step_num = 0

    def __iter__(self):
        return self

    def next_step(self):

        max_steps_in_stage = len(self.steps[self.current_stage])
        if self.current_step_num > max_steps_in_stage:
            raise StopIteration
        else:
            self.current_step_num += 1

    def next_stage(self):

        if self.current_stage_num == 4:
            raise StopIteration
        else:
            self.current_stage_num += 1

    def build_import_string(self, stage_name, step_num):
        import_string = ' '.join(['from ' + self.steps[stage_name][step_num]['package'],
                                  'import',
                                  self.steps[stage_name][step_num]['function']])
        return import_string

    def flatten_args(self, stage_name, step_num):

        arg_list = []
        for arg in range(0, len(self.steps[stage_name][step_num]['args'])):
            ky = list(self.steps[stage_name][step_num]['args'][arg].items())[0][0]
            vlu = list(self.steps[stage_name][step_num]['args'][arg].items())[0][1]
            if isinstance(vlu, str):
                vlu = '"'+vlu+'"'
            arg_list.append('='.join([ky, str(vlu)]))
        arg_string = ', '.join(arg_list)
        return arg_string

    def build_function_string(self, stage_name, step_num):

        function_string = ''.join([self.steps[stage_name][step_num]['function'],
                                    '(',
                                    self.flatten_args(stage_name, step_num),
                                    ')'])
        return function_string

if __name__ == "__main__":
    import os

    cfg_path = 'mac-testing-win.json'
    dir_path = os.path.dirname(os.path.realpath(__file__))
    os.chdir(dir_path)

    mod = Models(cfg_path)
    # print(mod.common_model_s3_key)
    mod.sim_tasks = []
    steps = Steps(cfg_path)
    runs = Runs(cfg_path)

    print(steps.flatten_args('setup', 0))

    # print(steps.build_function_string('setup', 0))
    #
    # # runs.sim_tasks = [[0 for x in range(runs.max_realizations)] for y in range(runs.max_iterations)]
    # # runs.sim_tasks[0][0] = 'blah'
    #
    # steps_list = []
    # for s in range(steps.num_sim_steps):
    #     tmp_iter_list = []
    #     for i in range(runs.max_iterations):
    #         tmp_sim_list = []
    #         for r in range(runs.max_realizations):
    #             tmp_sim_list.append('s=%s i=%s r=%s' % (s, i, r))
    #         tmp_iter_list.append(tmp_sim_list)
    #     steps_list.append(tmp_iter_list)
    #
    # print(steps_list)
    #
    #
    # print(steps_list[1][2][3])
    # print(runs.sim_tasks)



