# # General Imports
# import sys
# import os
# import json
#
# # Airflow imports
# from airflow.models import DAG
# from airflow.operators.python_operator import PythonOperator
#
# # Custom packages
#
# #     TODO Change this to efs volume
#
# # sys.path.append(os.path.expandvars("$AIRFLOW_HOME/dags/"))
# sys.path.append('/Users/robcannon/PycharmProjects/terraflowdags')
# from dag_helpers.config import parse_config_to_obj
# from sims.sim_call import sim_call
# from updates.update_call import update_call
#
# config_path = '%CONFIG_PATH%'
#
# if config_path[:1] == '%':
#     config_path = 'testing-win.json'
#
# print "Reading config from " + os.path.expandvars("$AIRFLOW_HOME/dags/efs-dags/" + config_path)
# with open(os.path.expandvars("$AIRFLOW_HOME/dags/efs-dags/" + config_path)) as json_data:
#     config = json.load(json_data)
#
# cfg, realization, iteration, model, simulator, update, run = parse_config_to_obj(config)
#
# args = {
#     'owner': 'airflow',
#     'start_date':run.start_date,
#     'schedule_interval':run.schedule_interval
# }
#
#
# dag = DAG(
#     dag_id=run.id,
#     default_args=args
# )
#
# # The order goes realization r then agg i
# # Schedule during creation
# # i for iterations
# # r for realizations
#
# sim_tasks = []
# agg_tasks = []
#
# sim_tasks_pre = []
# sim_tasks_post = []
#
# agg_tasks_pre = []
# agg_tasks_post = []
#
#
#
# for i in range(1, iteration.max + 1):
#     """
#     Build list of lists storing task objects to be scheduled in a later loop.
#     """
#     # Aggregation loop
#     if i >= iteration.current:
#         temp_sim_tasks = []
#         # Temporary list of sims
#         for r in range(1, realization.max + 1):
#             # Realization loop
#             if r >= realization.current or i > iteration.current:
#                 temp_sim_tasks.append(PythonOperator(
#                     task_id='sim-task-' + str(r) + '-agg-task-' + str(i),
#                     python_callable=sim_call(cfg, i, r),
#                     # params=params,
#                     # queue=queue,
#                     dag=dag
#                 ))
#
#         # List of lists stored for later reference
#         sim_tasks.append(temp_sim_tasks)
#         # Single list for all agg tasks
#         if i != iteration.max:
#             agg_tasks.append(PythonOperator(
#                 task_id='agg-task-' + str(i),
#                 python_callable=update_call(update),
#                 # params='',
#                 dag=dag
#             ))
#
# # Schedule list of lists within each loop
# for i in range(1, iteration.max + 1 - iteration.current):
#     # Iteration loop
#     for r in range(1, realization.max + 1):
#         # Realization loop
#         if not r > realization.max - realization.current - 1 or i > 1:
#             print str(r) + ' Reallization upstream of ' + str(i) + ' Iteration'
#             # Counter starts at 0, sim tasks set upstream of agg tasks in same iter
#             sim_tasks[i - 1][r - 1].set_downstream(agg_tasks[i - 1])
#             if i != iteration.max:
#                 # Stop the process from doing a last sim
#                 print 'Sim DS ' + str(r) + ' Real + ' + str(i) + ' Iter'
#                 # Agg tasks from the current iteration are set downstream of the next simulation run
#                 agg_tasks[i - 1].set_downstream(sim_tasks[i][r - 1])
#                 print agg_tasks[i - 1].task_id
#
