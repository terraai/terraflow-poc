
class Project(object):
    def __init__(self, cfg_path):
        import json
        with open(cfg_path) as json_data:
            config = json.load(json_data)
        self.project_name = config['project']['name']
        self.cfg = config

        self.directory = config['project']['directory']


    def import_s3(self):
        print(self.directory)


class Model(Project):

    def __init__(self, project):
        self.project = project
        self.model_name = project.cfg['model']['name']
        self.directory = project.directory + 'here'

    def __getattr__(self, item):
        return getattr(self.project, item)


class Sources(Model):
    def __init__(self, __model):
        self.__model = __model

    def __getattr__(self, item):
        return getattr(self.__model, item)


class SimulationDeck(Sources):
    def __init__(self, __sources):
        self.__sources = __sources

    def __getattr__(self, item):
        return getattr(self.__sources, item)


class EclipseDeck(SimulationDeck):

    def __init__(self, __simulation_deck):
        self.__simulation_deck = __simulation_deck

    def __getattr__(self, item):
        return getattr(self.__sources, item)


class Run(Model):

    def __init__(self, __model):
        self.__model = __model
        self.run_id = __model.cfg['run']['id']

        self.directory = 'here'

    def __getattr__(self, item):
        return getattr(self.__model, item)


class Stage(Run):

    def __init__(self, _run):
        self._run = _run

    def __getattr__(self, item):
        return getattr(self._run, item)

class Realization(Model):

    def __init__(self, __model):
        self.__model = __model

    def __getattr__(self, item):
        return getattr(self.__model, item)


class Scenario(Model):

    def __init__(self, __model):
        self.__model = __model

    def __getattr__(self, item):
        return getattr(self.__model, item)


class Case(Scenario):

    def __init__(self, __scenario):
        self.__scenario = __scenario


class Ensemble(Case, Model):

    def __init__(self, __case, __model):
        self.__case = __case
        self.__model = __model

    def __getattr__(self, item):
        try:
            return getattr(self.__case, item)
        except AttributeError:
            return getattr(self.__model, item)


prj = Project('../mac-testing-win.json')
mod = Model(prj)
run = Run(mod)
stage = Stage(run)

print(prj.project_name)
print(mod.project_name)
print(mod.model_name)
print(run.run_id)
run.import_s3()
mod.prop = 1
print(mod.prop)
print(run.prop)
print(stage.run_id)
