

class Foo(object):
    def __init__(self, stuff=None):
        self.stuff = stuff
        # self.ntings = self._do_something()
        print('init here ')

    @property
    def _do_stuff(self):
        return self.stuff + ' and more '

    @property
    def other_stuff(self):
        return self._do_something()

    def __set__(self, instance, value):
        if self.stuff is None:
            raise AttributeError('Gimmme sturff!!')

    def _do_something(self):
        print('doing something')
        return self.stuff + ' things'


class Boo(Foo):
    def __init__(self, stuff):
        super().__init__(stuff)
        self.stuff = 'boo stuff'
        print('Boo init')

    @property
    def other_stuff(self):
        return self._do_something()

class Wee(Boo):
    def __init__(self, stuff):
        super().__init__(stuff)
        self.stuff = 'wee stuff'
        # self.things = self._do_stuff

    @property
    def other_stuff(self):
        return self._do_something()



# xxx = Wee('Plop')
# print(xxx.other_stuff)

class FooFoo(object):
    def __init__(self, stuff=None):
        self.stuff = stuff
        self.other_stuff = self._do_something()
        print('init here ')

    @property
    def _do_stuff(self):
        return self.stuff + ' and more '

    def __set__(self, instance, value):
        if self.stuff is None:
            raise AttributeError('Gimmme sturff!!')

    def _do_something(self):
        print('doing something')
        return self.stuff + ' things'


class BooBoo(FooFoo):
    def __init__(self, stuff):
        super().__init__(stuff)
        self.stuff = 'boo stuff'
        self.other_stuff = self._do_something()
        print('Boo init')


class WeeWee(BooBoo):
    def __init__(self, stuff):
        super().__init__(stuff)
        self.stuff = 'wee stuff'
        self.other_stuff = self._do_something()


www = WeeWee('wee stuff')
print(www.other_stuff)
