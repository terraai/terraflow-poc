

class Project(object):
    def __init__(self, cfg_path):
        import json
        with open(cfg_path) as json_data:
            config = json.load(json_data)
        self.project_name = config['project']['name']
        self.cfg = config

        self.directory = config['project']['directory']


    def import_s3(self):
        print(self.directory)


class Model(Project):

    def __init__(self, project):
        self.project = project
        self.model_name = project.cfg['model']['name']
        self.directory = project.directory + 'here'

    def __getattr__(self, item):
        return getattr(self.project, item)


class Sources(Model):
    def __init__(self, __model):
        self.__model = __model

    def __getattr__(self, item):
        return getattr(self.__model, item)


class Ensemble(Sources):

    def __init__(self, __sources):
        self.__sources = __sources

    def __getattr__(self, item):
        return getattr(self.__sources, item)


class SimulationDeck(Sources):
    def __init__(self, __sources):
        self.__sources = __sources

    def __getattr__(self, item):
        return getattr(self.__sources, item)


class EclipseDeck(SimulationDeck):

    def __init__(self, __simulation_deck):
        self.__simulation_deck = __simulation_deck

    def __getattr__(self, item):
        return getattr(self.__sources, item)


class MRSTDeck(SimulationDeck):

    def __init__(self, __simulation_deck):
        self.__simulation_deck = __simulation_deck

    def __getattr__(self, item):
        return getattr(self.__sources, item)


class CMGDeck(SimulationDeck):

    def __init__(self, __simulation_deck):
        self.__simulation_deck = __simulation_deck

    def __getattr__(self, item):
        return getattr(self.__sources, item)


class Run(Model):

    def __init__(self, __model):
        self.__model = __model
        self.run_id = __model.cfg['run']['id']

        self.directory = 'here'

    def __getattr__(self, item):
        return getattr(self.__model, item)


class Iteration(Run):

    def __init__(self, _run):
        self._run = _run

    def __getattr__(self, item):
        return getattr(self._run, item)


class Realization(Ensemble, Iteration):

    def __init__(self, __ensemble, __iteration):
        self.__ensemble = __ensemble
        self.__iteration = __iteration

    def __getattr__(self, item):
        try:
            return getattr(self.__ensemble, item)
        except AttributeError:
            return getattr(self.__iteration, item)


class Stage(Run):

    def __init__(self, _run):
        self._run = _run

    def __getattr__(self, item):
        return getattr(self._run, item)


class Step(Stage):

    def __init__(self, __stage):
        self.__stage = __stage

    def __getattr__(self, item):
        return getattr(self.__stage, item)


class Schedule(Step):

    def __init__(self, __step):
        self.__step = __step

    def __getattr__(self, item):
        return getattr(self.__step, item)


if __name__ == "__main__":

    # prj = Project('../mac-testing-win.json')
    # mod = Model(prj)
    # run = Run(mod)
    # stage = Stage(run)

    prj = Project(cfg_path='../mac-testing-win.json')
    mod = Model(prj)

    run = Run(mod)
    stage = Stage(run)
    step = Step(stage)
    sch = Schedule(step)

    src = Sources(mod)
    ens = Ensemble(src)

    simd = SimulationDeck(src)
    ecl = EclipseDeck(simd)

    itr = Iteration(run)
    real = Realization(ens, itr)

    print(prj.project_name)
    print(mod.project_name)
    print(mod.model_name)
    print(run.run_id)
    run.import_s3()
    mod.prop = 1
    print(mod.prop)
    print(run.prop)
    print(stage.run_id)

else:
    prj = Project(cfg_path='../mac-testing-win.json')
    mod = Model(prj)

    run = Run(mod)
    stage = Stage(run)
    step = Step(stage)
    sch = Schedule(step)

    src = Sources(mod)
    ens = Ensemble(src)

    simd = SimulationDeck(src)
    ecl = EclipseDeck(simd)

    itr = Iteration(run)
    real = Realization(ens, itr)

