

class Project(object):
    def __init__(self, cfg_path):
        import json
        with open(cfg_path) as json_data:
            config = json.load(json_data)
        self.project_name = config['project']['name']
        self.cfg = config

        self.project_name = config['project']['name']
        self.base_dir = config['project']['directory']
        self.dir = '/'.join([self.base_dir, self.project_name])

        self.bucket = 'terraai-projects'

    def import_from_s3(self):
        import os
        os.system(' '.join(['aws s3 cp', self.]))

    def create_new_local(self):
        import os
        os.system('mkdir -p ')

    def export


class Model(Project):

    def __init__(self, project):
        self.project = project
        self.model_name = project.cfg['model']['name']
        self.directory = project.directory + 'here'

    def __getattr__(self, item):
        return getattr(self.project, item)


class Run(Model):

    def __init__(self, __model):
        self.__model = __model
        self.run_id = __model.cfg['run']['id']

        self.directory = 'here'

    def __getattr__(self, item):
        return getattr(self.__model, item)


class Iteration(Run):

    def __init__(self, _run):
        self._run = _run

    def __getattr__(self, item):
        return getattr(self._run, item)


class Realization(Iteration):

    def __init__(self, __iteration):
        self.__iteration = __iteration

    def __getattr__(self, item):
        try:
            return getattr(self.__iteration, item)
        except AttributeError:
            pass


class Stage(Run):

    def __init__(self, _run):
        self._run = _run

    def __getattr__(self, item):
        return getattr(self._run, item)

    @property
    def stage_name(self):
        return self.


class Step(Stage):

    def __init__(self, __stage):
        self.__stage = __stage
        


    def __getattr__(self, item):
        return getattr(self.__stage, item)


class Schedule(Step):

    def __init__(self, __step):
        self.__step = __step

    def __getattr__(self, item):
        return getattr(self.__step, item)


if __name__ == "__main__":

    # prj = Project('../mac-testing-win.json')
    # mod = Model(prj)
    # run = Run(mod)
    # stage = Stage(run)

    prj = Project(cfg_path='../mac-testing-win.json')
    mod = Model(prj)

    run = Run(mod)
    stage = Stage(run)
    step = Step(stage)
    sch = Schedule(step)

    itr = Iteration(run)
    real = Realization(itr)

    print(prj.project_name)
    print(mod.project_name)
    print(mod.model_name)
    print(run.run_id)
    run.import_s3()
    mod.prop = 1
    print(mod.prop)
    print(run.prop)
    print(stage.run_id)

else:
    prj = Project(cfg_path='../mac-testing-win.json')
    mod = Model(prj)

    run = Run(mod)
    stage = Stage(run)
    step = Step(stage)
    sch = Schedule(step)

    src = Sources(mod)
    ens = Ensemble(src)

    simd = SimulationDeck(src)
    ecl = EclipseDeck(simd)

    itr = Iteration(run)
    real = Realization(ens, itr)

