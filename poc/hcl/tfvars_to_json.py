import hcl
import os
import json


varsfile = 'terraform.tfvars'
obj = {}
if os.path.exists(varsfile):
    with open(varsfile, 'r') as fp:
        obj = hcl.load(fp)
print(obj)

with open('test.json', 'w') as outfile:
    json.dump(obj, outfile)


varsfile = 'deployment.tfvars'
obj = {}
if os.path.exists(varsfile):
    with open(varsfile, 'r') as fp:
        obj = hcl.load(fp)
print(obj)

with open('deploy.tfvars', 'w') as outfile:
    json.dump(obj, outfile)