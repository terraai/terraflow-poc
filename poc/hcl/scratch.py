import hcl
from pathlib import Path
import os
import json

comp_path = Path(os.path.join(Path(__file__).parents[1], 'iam/ecs_instance.tf'))

comp_dir = os.listdir(Path(os.path.join(Path(__file__).parents[1], 'iam')))

print(comp_dir)

obj = {}
if os.path.exists(comp_path):
    with open(comp_path, 'r') as fp:
        obj = hcl.load(fp)
print(obj)

with open('test.json', 'w') as outfile:
    json.dump(obj, outfile)