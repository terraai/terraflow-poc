"terragrunt" "terraform" {
  "source" = "git::git@github.com:foo/modules.git//frontend-app?ref=v0.0.3"

  "extra_arguments" "custom_vars" {
    "commands" = ["apply", "plan", "import", "push", "refresh"]

    "arguments" = ["-var-file=${get_tfvars_dir()}/../common.tfvars", "-var-file=terraform.tfvars"]
  }
}