
provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {}
}

resource "aws_iam_role" "ecs_instance_role" {
  name = "${format("%s-%s", var.environment, "ecs-instance-role")}"
  assume_role_policy = "${file(${path.module},"/roles/", ${var.role}, ".json")}"
}

resource "aws_iam_role_policy" "ecs_instance_role_policy" {
  name = "${format("%s-%s", var.environment, "ecs-instance-role-policy")}"
  policy = "${file("policies/ecs-instance-role-policy.json")}"

  role = "${aws_iam_role.ecs_instance_role.id}"
}

resource "aws_iam_role_policy_attachment" "ecs_ec2_cloudwatch_role" {
  role = "${aws_iam_role.ecs_instance_role.id}"
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchLogsFullAccess"
}
