
terragrunt = {
  terraform {
    source = "../../../../../../modules//terraflow"
  }
  include = {
    path = "${find_in_parent_folders()}"
  }

}

cloudwatch_prefix = "terraflow"

name = "terraflow"

instance_type = "t2.medium"

volume_size = 8

policy = "ecs-instance-role-policy"

role = "ecs-instance-role"