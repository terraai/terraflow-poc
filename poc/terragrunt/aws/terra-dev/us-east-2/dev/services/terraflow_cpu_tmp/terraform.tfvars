
terragrunt = {
  terraform {
    source = "../../../../../../modules//terraflow_cpu_temp"
  }
  include = {
    path = "${find_in_parent_folders()}"
  }
}

name = "terraflow_cpu_tmp"

queue = "cpusims"

cloudwatch_prefix = "terraflow_cpu_tmp"

instance_type = "c4.2xlarge"

instance_count = 2

spot_price = ".9"

volume_size = 8

//user_data_file = "terraflow-be.sh"

policy = "ecs-instance-role-policy"

role = "ecs-instance-role"