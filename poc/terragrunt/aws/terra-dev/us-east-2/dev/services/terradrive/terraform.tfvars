
terragrunt = {
  terraform {
    source = "../../../../../../modules//terradrive"
  }
  include = {
    path = "${find_in_parent_folders()}"
  }

}

name = "terradrive-manifold"

