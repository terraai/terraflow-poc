terragrunt = {
  terraform {
    source = "../../../../../../modules//vpc_master"
  }
  include = {
    path = "${find_in_parent_folders()}"
  }
}

vpc_cidr = "10.10.0.0/16"

availability_zones = [
  "us-east-2a",
  "us-east-2b"]

public_subnet_cidrs = [
  "10.10.11.0/24",
  "10.10.12.0/24"]
private_subnet_cidrs = [
  "10.10.21.0/24",
  "10.10.22.0/24"]
database_subnets_cidrs = [
  "10.10.41.0/24",
  "10.10.42.0/24"]
redshift_subnets_cidrs = [
  "10.10.51.0/24",
  "10.10.52.0/24"]
elasticache_subnets_cidrs = [
  "10.10.61.0/24",
  "10.10.62.0/24"]
