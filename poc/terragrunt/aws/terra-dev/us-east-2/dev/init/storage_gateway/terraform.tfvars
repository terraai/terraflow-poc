terragrunt = {
  terraform {
    source = "../../../../../../modules//storage_gateway"
  }

  include = {
    path = "${find_in_parent_folders()}"
  }

  dependencies {
    paths = [
      "../vpc_main", "../security_groups"]
  }
}
