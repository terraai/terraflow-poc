terragrunt = {
  terraform {
    source = "../../../../../modules//vpc-peering"
  }
  include = {
    path = "${find_in_parent_folders()}"
  }
}
