terragrunt = {
  terraform {
    source = "../../../../../../modules//rds"
  }

  include {
    path = "${find_in_parent_folders()}"
  }

  dependencies {
    paths = [
      "../vpc_main", "../security_groups"]
  }
}

instance_class = "db.t2.micro"
