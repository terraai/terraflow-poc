terragrunt = {
  terraform {
    source = "../../../../../../modules//bastion"
  }

  include {
    path = "${find_in_parent_folders()}"
  }

  dependencies {
    paths = [
      "../vpc_main",
      "../security_groups"]
  }
}

