terragrunt = {
  remote_state {
    backend = "s3"
    config {
      bucket         = "terranet-states"
      key            = "${path_relative_to_include()}/terraform.tfstate"
      region         = "us-east-2"
      encrypt        = true
      dynamodb_table = "terradev-lock-table"
    }
  }
    terraform {
    extra_arguments "required_vars" {
      commands = ["${get_terraform_commands_that_need_vars()}"]

      required_var_files = [
        "${get_tfvars_dir()}/${find_in_parent_folders("account.tfvars")}",
        "${get_tfvars_dir()}/${find_in_parent_folders("region.tfvars")}",
        "${get_tfvars_dir()}/${find_in_parent_folders("env.tfvars")}"
      ]

      optional_var_files = [
        "${get_tfvars_dir()}/${find_in_parent_folders("init.tfvars", "ignore")}",
        "${get_tfvars_dir()}/${find_in_parent_folders("services.tfvars", "ignore")}",
        "${get_tfvars_dir()}/${find_in_parent_folders("terraflow.tfvars", "ignore")}",
      ]
    }
  }
}