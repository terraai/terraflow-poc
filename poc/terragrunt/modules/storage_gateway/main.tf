
//TODO
// 1. Make new bucket
// 2. Make policies that the storage gateway can mount - can do in console
// 3. Use output IP within userdata of mounts in terraflow


provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {}
}

resource "aws_key_pair" "key_pair" {
  public_key = "${file("${path.module}/storage_gateway.pub")}"
}

resource "aws_spot_instance_request" "storage_gateway" {
//  ami = "ami-7845741d"
  ami = "ami-072b86d4df1352083"

  instance_type = "m5d.large"
  spot_price = ".13"

  key_name = "${aws_key_pair.key_pair.key_name}"

  vpc_security_group_ids = ["${data.terraform_remote_state.security_groups.nfs_sg_id}"]
  subnet_id = "${data.terraform_remote_state.vpc_main.public_subnets[0]}"
}




data "terraform_remote_state" "security_groups" {
  backend = "s3"
  config {
    bucket = "${var.remote_state_bucket}"
    key    = "${var.region}/${var.environment}/init/security_groups/terraform.tfstate"
    region = "${var.region}"
  }
}

data "terraform_remote_state" "vpc_main" {
  backend = "s3"
  config {
    bucket = "${var.remote_state_bucket}"
    key    = "${var.region}/${var.environment}/init/vpc_main/terraform.tfstate"
    region = "${var.region}"
  }
}
