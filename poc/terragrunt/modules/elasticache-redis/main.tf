

resource "aws_elasticache_cluster" "redis" {
  cluster_id           = "redis"
  engine               = "redis"
  node_type            = "cache.t2.micro"
  num_cache_nodes      = 1
  parameter_group_name = "default.redis3.2"
  port                 = 6379

  security_group_ids = ["${aws_security_group.cellspace_root_sg.id}"]
  subnet_group_name = "${module.vpc.elasticache_subnet_group_name}"
}

data "aws_elasticache_cluster" "redis" {
  cluster_id = "${aws_elasticache_cluster.redis.cluster_id}"
}

resource "aws_route53_zone" "terraflow-redis" {
  name = "${var.environment}-terraflow-redis.terra-ai.net"
}

resource "aws_route53_record" "terraflow-redis" {
  count = 1
  name = "${var.environment}-terraflow-redis"
  zone_id = "${aws_route53_zone.terraflow-redis.id}"
  type = "CNAME"
  ttl = 60
  records = ["${data.aws_elasticache_cluster.redis.cache_nodes.0.address}"]
}

//resource "aws_route53_zone" "redis" {
//  name = "${var.environment}-redis.terra-ai.net"
//}
//
//resource "aws_route53_record" "" {
//  name = "redis"
//  type = "A"
//  zone_id = "${aws_route53_zone.redis.id}"
//  records = ["$"]
//}

