//        Plan
//        1. create snapshot before destroy - option in rds
//        2. tag that snapshot for application - has snapshot identifier option
//        3. reference that snapshot with a data source - data source
//        4. have script run each load to perform basic setup actions - for setup in af
//        5. make all maintenance and setup through parameter group - ??
//        6. lock down secrets - ??


provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {}
}


resource "aws_db_instance" "this" {
  name = "terraflow"
  identifier = "terraflow"

  //  General
  engine = "postgres"
  engine_version = "9.5.4"
  port = 5432
  instance_class = "${var.instance_class}"

  //  Networking
  vpc_security_group_ids = ["${data.terraform_remote_state.security_groups.database_sg_id}"]
  db_subnet_group_name = "${data.terraform_remote_state.vpc_main.database_subnet_group}"
  multi_az = false
  publicly_accessible = true

  //  Credentials
  password = "${var.password}"
  username = "${var.username}"

  // Storage
  allocated_storage = 10
  # gigabytes
  backup_retention_period = 7
  # in days
  //  TODO enable encryption
  storage_encrypted = false
  # When sizing up, this can be enabled. Just not in t2.micro
  storage_type = "gp2"

  //  Snapshot
  skip_final_snapshot = true
}

//data "aws_db_snapshot" "terraflow db" {
//  db_instance_identifier = "${aws_db_instance.terraflow-rds.id}"
////  TODO make this dynamic
//  db_snapshot_identifier = ""
//

//provider "postgresql" {
//  host = "${aws_db_instance.this.address}"
//  port = 5432
//  database = "${var.database}"
//  username = "${var.username}"
//  password = "${var.password}"
//  //  sslmode         = "require"
//  connect_timeout = 15
//}
//
//resource "postgresql_role" "terraflow_role" {
//  provider = "postgresql"
//  name = "terraflow_admin"
//  login = true
//  password = "terraflow_admin"
//}

data "terraform_remote_state" "security_groups" {
  backend = "s3"
  config {
    bucket = "${var.remote_state_bucket}"
    key    = "${var.region}/${var.environment}/init/security_groups/terraform.tfstate"
    region = "${var.region}"
  }
}

data "terraform_remote_state" "vpc_main" {
  backend = "s3"
  config {
    bucket = "${var.remote_state_bucket}"
    key    = "${var.region}/${var.environment}/init/vpc_main/terraform.tfstate"
    region = "${var.region}"
  }
}
