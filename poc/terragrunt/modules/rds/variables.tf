


variable "remote_state_bucket" {}

variable "region" {}

variable "environment" {}

variable "instance_class" {}

variable "database" {}

variable "username" {}

variable "password" {}