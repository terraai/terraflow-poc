
provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {}
}

locals {
  name = "${var.environment}-${var.name}"
}

data "template_file" "iam_role" {
  template = "${file("${var.role}")}"
//  TODO insert vars
}

resource "aws_iam_role" "this" {
  name = "${local.name}-role"
//  assume_role_policy = "${data.template_file.iam_role.rendered}"
  assume_role_policy = "${file("${var.role}")}"
}

//data "template_file" "iam_policy" {
//  template = "${file("${var.policy}")}"
////  TODO insert vars
//}

resource "aws_iam_role_policy" "this" {
  name = "${local.name}-policy"
//  policy = "${data.template_file.iam_policy.rendered}"
  policy = "${file("${var.policy}")}"

  role = "${aws_iam_role.this.id}"
}

resource "aws_iam_role_policy_attachment" "ecs_ec2_cloudwatch_role" {
  role = "${aws_iam_role.this.id}"
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchLogsFullAccess"
}
