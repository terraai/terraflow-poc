
//  ###################################################################################################################
//                                                                                     Database
//  ###################################################################################################################

resource "aws_security_group" "database_sg" {
  name   = "${var.environment}-database-sg"
  vpc_id = "${data.terraform_remote_state.vpc_main.vpc_id}"
  //  description = "database sg"
  tags {
    Name = "database-sg"
  }
}

//  ###################################################################################################################
//                                                                                     Ingress
//  ###################################################################################################################

resource "aws_security_group_rule" "db-self-in" {
  security_group_id = "${aws_security_group.database_sg.id}"
  from_port = 0
  to_port   = 65535
  protocol  = "tcp"
  type      = "ingress"
  self = true
}

// TODO Unnecesary? - Jump server
resource "aws_security_group_rule" "db-cip-in" {
  security_group_id = "${aws_security_group.database_sg.id}"
  from_port = 5432
  to_port   = 5432
  protocol  = "-1"
  type      = "ingress"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "db-bastion-in" {
  security_group_id = "${aws_security_group.database_sg.id}"
  from_port = 22
  to_port   = 22
  protocol  = "tcp"
  type      = "ingress"
  source_security_group_id = "${aws_security_group.bastion_sg.id}"
}

resource "aws_security_group_rule" "db-bastion-pg-in" {
  security_group_id = "${aws_security_group.database_sg.id}"
  from_port = 5432
  to_port   = 5432
  protocol  = "-1"
  type      = "ingress"
  source_security_group_id = "${aws_security_group.bastion_sg.id}"
}

resource "aws_security_group_rule" "db-www-pg-in" {
  security_group_id = "${aws_security_group.database_sg.id}"
  from_port = 5432
  to_port   = 5432
  protocol  = "-1"
  type      = "ingress"
  source_security_group_id = "${aws_security_group.www_sg.id}"
}

resource "aws_security_group_rule" "db-workers-pg-in" {
  security_group_id = "${aws_security_group.database_sg.id}"
  from_port = 5432
  to_port   = 5432
  protocol  = "-1"
  type      = "ingress"
  source_security_group_id = "${aws_security_group.workers_sg.id}"
}


//  ###################################################################################################################
//                                                                                     Egress
//  ###################################################################################################################

// TODO Lock down
resource "aws_security_group_rule" "db-workers-out" {
  security_group_id = "${aws_security_group.database_sg.id}"
  from_port = 0
  to_port   = 65535
  protocol  = "-1"
  type      = "egress"
  cidr_blocks = ["0.0.0.0/0"]
}
