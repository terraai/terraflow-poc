
//  ###################################################################################################################
//                                                                                     WWW
//  ###################################################################################################################

resource "aws_security_group" "www_sg" {
  name   = "${var.environment}-www-sg"
  vpc_id = "${data.terraform_remote_state.vpc_main.vpc_id}"
  //  description = "frontend sg"
  tags {
    Name = "Frontend sg"
  }
}


//  ###################################################################################################################
//                                                                                     Ingress
//  ###################################################################################################################


// TODO May not need unless LB is in this SG
resource "aws_security_group_rule" "www-self-in" {
  security_group_id = "${aws_security_group.www_sg.id}"
  from_port = 0
  to_port   = 65355
  protocol  = "tcp"
  type      = "ingress"
  self = true
}

//TODO Tenmp
resource "aws_security_group_rule" "www-all-in" {
  security_group_id = "${aws_security_group.www_sg.id}"
  from_port = 0
  to_port   = 65355
  protocol  = "-1"
  type      = "ingress"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "www-https-in" {
  security_group_id = "${aws_security_group.www_sg.id}"
  from_port = 443
  to_port   = 443
  protocol  = "tcp"
  type      = "ingress"
  cidr_blocks = ["0.0.0.0/0"]
}

//  ###################################################################################################################
//                                                                                     Egress
//  ###################################################################################################################

// TODO Might need additional egress rules - only letting http/s out
resource "aws_security_group_rule" "www-http-out" {
  security_group_id = "${aws_security_group.www_sg.id}"
  //  description = "www to workers"
  from_port = 80
  to_port   = 80
  protocol  = "tcp"
  type      = "egress"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "www-https-out" {
  security_group_id = "${aws_security_group.www_sg.id}"
  //  description = "www to workers"
  from_port = 443
  to_port   = 443
  protocol  = "tcp"
  type      = "egress"
  cidr_blocks = ["0.0.0.0/0"]
}

// TODO Might not need
resource "aws_security_group_rule" "www-workers-in" {
  security_group_id = "${aws_security_group.www_sg.id}"
  //  description = "www to workers"
  from_port = 0
  to_port   = 65355
  protocol  = "tcp"
  type      = "ingress"
  source_security_group_id = "${aws_security_group.workers_sg.id}"
}

resource "aws_security_group_rule" "www-nfs-in" {
  security_group_id = "${aws_security_group.www_sg.id}"
  //  description = "www to nfs"
  from_port = 2049
  to_port   = 2049
  protocol  = "tcp"
  type      = "ingress"
  source_security_group_id = "${aws_security_group.nfs_sg.id}"
}

resource "aws_security_group_rule" "www-db-out" {
  security_group_id = "${aws_security_group.www_sg.id}"
  //  description = "www to db"
  from_port = 0
  to_port   = 65355
  protocol  = "-1"
  type      = "egress"
  source_security_group_id = "${aws_security_group.database_sg.id}"
}

// TODO Might not need with LB
resource "aws_security_group_rule" "www-all-out" {
  security_group_id = "${aws_security_group.www_sg.id}"
  //  description = "www to db"
  from_port = 0
  to_port   = 65355
  protocol  = "-1"
  type      = "egress"
  cidr_blocks = ["0.0.0.0/0"]
}
