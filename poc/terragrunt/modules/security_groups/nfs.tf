

//  ###################################################################################################################
//                                                                                     NFS
//  ###################################################################################################################

resource "aws_security_group" "nfs_sg" {
  name   = "${var.environment}-nfs-sg"
  vpc_id = "${data.terraform_remote_state.vpc_main.vpc_id}"
  //  description = "nfs ingesters sg"
  tags {
    Name = "NFS ingesters sg"
  }
}


//  ###################################################################################################################
//                                                                                     Ingress
//  ###################################################################################################################


resource "aws_security_group_rule" "nfs-bastion-in" {
  security_group_id = "${aws_security_group.nfs_sg.id}"
  //  description = "nfs to csr"
  from_port = 22
  to_port   = 22
  protocol  = "tcp"
  type      = "ingress"
  source_security_group_id = "${aws_security_group.bastion_sg.id}"
}

//TODO Testing
//resource "aws_security_group_rule" "nfs-all-in" {
//  security_group_id = "${aws_security_group.nfs_sg.id}"
//  //  description = "nfs to www"
//  from_port = 80
//  to_port   = 80
//  protocol  = "-1"
//  type      = "ingress"
//  cidr_blocks = ["0.0.0.0/0"]
//}

resource "aws_security_group_rule" "nfs-cip-in" {
  security_group_id = "${aws_security_group.nfs_sg.id}"
  //  description = "nfs to www"
  from_port = 2049
  to_port   = 2049
  protocol  = "-1"
  type      = "ingress"
  cidr_blocks = ["${var.corporate_ip}"]
}

resource "aws_security_group_rule" "nfs-www-in" {
  security_group_id = "${aws_security_group.nfs_sg.id}"
  //  description = "nfs to workers"
  from_port = 2049
  to_port   = 2049
  protocol  = "tcp"
  type      = "ingress"
  source_security_group_id = "${aws_security_group.www_sg.id}"
}

resource "aws_security_group_rule" "nfs-workers-in" {
  security_group_id = "${aws_security_group.nfs_sg.id}"
  //  description = "nfs to workers"
  from_port = 2049
  to_port   = 2049
  protocol  = "tcp"
  type      = "ingress"
  source_security_group_id = "${aws_security_group.workers_sg.id}"
}

//TODO Testing - rm
//resource "aws_security_group_rule" "nfs-all-in" {
//  security_group_id = "${aws_security_group.nfs_sg.id}"
//  //  description = "nfs to workers"
//  from_port = 0
//  to_port   = 65355
//  protocol  = "-1"
//  type      = "ingress"
//  cidr_blocks = ["0.0.0.0/0"]
//}


//  ###################################################################################################################
//                                                                                     Egress
//  ###################################################################################################################

resource "aws_security_group_rule" "nfs-all-out" {
  security_group_id = "${aws_security_group.nfs_sg.id}"
  //  description = "nfs to www"
  from_port = 0
  to_port   = 65355
  protocol  = "-1"
  type      = "egress"
  cidr_blocks = ["0.0.0.0/0"]
}
//
//resource "aws_security_group_rule" "nfs-workers-www-out" {
//  security_group_id = "${aws_security_group.nfs_sg.id}"
//  //  description = "nfs to workers"
//  from_port = 2049
//  to_port   = 2049
//  protocol  = "-1"
//  type      = "egress"
//  source_security_group_id = "${aws_security_group.www_sg.id}"
//}
//
//resource "aws_security_group_rule" "nfs-workers-workers-out" {
//  security_group_id = "${aws_security_group.nfs_sg.id}"
//  //  description = "nfs to workers"
//  from_port = 2049
//  to_port   = 2049
//  protocol  = "-1"
//  type      = "egress"
//  source_security_group_id = "${aws_security_group.workers_sg.id}"
//}


////TODO Testing - rm
//resource "aws_security_group_rule" "nfs-all-out" {
//  security_group_id = "${aws_security_group.nfs_sg.id}"
//  //  description = "nfs to workers"
//  from_port = 2049
//  to_port   = 2049
//  protocol  = "-1"
//  type      = "egress"
//  cidr_blocks = ["0.0.0.0/0"]
//}
