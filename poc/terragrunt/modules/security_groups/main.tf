
//Security Groups
//1. bastion
//2. frontend
//3. backend
//4.

provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {}
}


data "terraform_remote_state" "vpc_mgmt" {
  backend = "s3"
  config {
    bucket = "${var.remote_state_bucket}"
    key    = "${var.region}/${var.environment}/init/vpc_mgmt/terraform.tfstate"
    region = "${var.region}"
  }
}

data "terraform_remote_state" "vpc_main" {
  backend = "s3"
  config {
    bucket = "${var.remote_state_bucket}"
    key    = "${var.region}/${var.environment}/init/vpc_main/terraform.tfstate"
    region = "${var.region}"
  }
}

