
output "bastion_sg_id" {
  value = "${aws_security_group.bastion_sg.id}"
}

output "www_sg_id" {
  value = "${aws_security_group.www_sg.id}"
}

output "nfs_sg_id" {
  value = "${aws_security_group.nfs_sg.id}"
}

output "workers_sg_id" {
  value = "${aws_security_group.workers_sg.id}"
}

output "database_sg_id" {
  value = "${aws_security_group.database_sg.id}"
}