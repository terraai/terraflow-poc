



//  ###################################################################################################################
//                                                                                     Bastion
//  ###################################################################################################################

resource "aws_security_group" "bastion_sg" {
  name   = "${var.environment}-bastion-sg"
  vpc_id = "${data.terraform_remote_state.vpc_main.vpc_id}"
  //  description = "bastion sg"
  tags {
    Name = "bastion-sg"
  }
}

//  ###################################################################################################################
//                                                                                     Ingress
//  ###################################################################################################################

resource "aws_security_group_rule" "bastion-ssh-in" {
  security_group_id = "${aws_security_group.bastion_sg.id}"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  type              = "ingress"
  cidr_blocks       = ["${var.corporate_ip}"]
}

//  ###################################################################################################################
//                                                                                     Egress
//  ###################################################################################################################

resource "aws_security_group_rule" "bastion-www-out" {
  security_group_id = "${aws_security_group.bastion_sg.id}"
  from_port         = 22
  to_port           = 22
  protocol          = "-1"
  type              = "egress"
  source_security_group_id = "${aws_security_group.www_sg.id}"
}

resource "aws_security_group_rule" "bastion-workers-out" {
  security_group_id = "${aws_security_group.bastion_sg.id}"
  from_port         = 22
  to_port           = 22
  protocol          = "-1"
  type              = "egress"
  source_security_group_id = "${aws_security_group.workers_sg.id}"
}

resource "aws_security_group_rule" "bastion-nfs-out" {
  security_group_id = "${aws_security_group.bastion_sg.id}"
  from_port         = 22
  to_port           = 22
  protocol          = "-1"
  type              = "egress"
  source_security_group_id = "${aws_security_group.nfs_sg.id}"
}

resource "aws_security_group_rule" "bastion-db-out" {
  security_group_id = "${aws_security_group.bastion_sg.id}"
  from_port         = 22
  to_port           = 22
  protocol          = "-1"
  type              = "egress"
  source_security_group_id = "${aws_security_group.database_sg.id}"
}
