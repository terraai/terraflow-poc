

//  ###################################################################################################################
//                                                                                     workers
//  ###################################################################################################################

resource "aws_security_group" "workers_sg" {
  name   = "${var.environment}-workers-sg"
  vpc_id = "${data.terraform_remote_state.vpc_main.vpc_id}"
  //  description = "workers sg"
  tags {
    Name = "workers-sg"
  }
}

//  ###################################################################################################################
//                                                                                     Ingress
//  ###################################################################################################################

resource "aws_security_group_rule" "workers-self-in" {
  security_group_id = "${aws_security_group.workers_sg.id}"
  //  description = "workers to csr"
  from_port = 0
  to_port   = 65535
  protocol  = "tcp"
  type      = "ingress"
  self = true
}

resource "aws_security_group_rule" "workers-bastion-in" {
  security_group_id = "${aws_security_group.workers_sg.id}"
  //  description = "workers to csr"
  from_port = 22
  to_port   = 22
  protocol  = "tcp"
  type      = "ingress"
  source_security_group_id = "${aws_security_group.bastion_sg.id}"
}

resource "aws_security_group_rule" "workers-www-in" {
  security_group_id = "${aws_security_group.workers_sg.id}"
  from_port = 0
  to_port   = 65535
  protocol  = "tcp"
  type      = "ingress"
  source_security_group_id = "${aws_security_group.www_sg.id}"
}

//  ###################################################################################################################
//                                                                                     Egress
//  ###################################################################################################################


// TODO Lock down
resource "aws_security_group_rule" "workers-out" {
  security_group_id = "${aws_security_group.workers_sg.id}"
  from_port = 0
  to_port   = 65535
  protocol  = "-1"
  type      = "egress"
  cidr_blocks = ["0.0.0.0/0"]
}

