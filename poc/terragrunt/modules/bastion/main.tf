
provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {}
}

resource "aws_key_pair" "default_key_pait" {
  public_key = "${file("${path.module}/cellspace-root.pub")}"
}

resource "aws_spot_instance_request" "bastion" {
  ami = "${var.bastion_ami}"
  instance_type = "t2.micro"
  vpc_security_group_ids = [
    "${data.terraform_remote_state.security_groups.bastion_sg_id}"]
  key_name = "${aws_key_pair.default_key_pait.key_name}"

  subnet_id = "${data.terraform_remote_state.vpc_main.public_subnets[0]}"

  root_block_device {
    volume_type = "gp2"
    volume_size = "8"
  }

  # TODO This should be dynamic
  volume_tags {
    Name = "bastion-root-ebs"
  }

  # TODO This should be dynamic
  tags {
    Name = "bastion-root"
  }

  spot_price = ".1"
}

resource "aws_eip" "bastion" {
  vpc = true
  instance = "${aws_spot_instance_request.bastion.spot_instance_id}"
  tags {
    name = "${var.environment}-bastion"
  }
}

data "terraform_remote_state" "security_groups" {
  backend = "s3"
  config {
    bucket = "${var.remote_state_bucket}"
    key    = "${var.region}/${var.environment}/init/security_groups/terraform.tfstate"
    region = "${var.region}"
  }
}

data "terraform_remote_state" "vpc_main" {
  backend = "s3"
  config {
    bucket = "${var.remote_state_bucket}"
    key    = "${var.region}/${var.environment}/init/vpc_main/terraform.tfstate"
    region = "${var.region}"
  }
}

//data "terraform_remote_state" "vpc_mgmt" {
//  backend = "s3"
//  config {
//    bucket = "${var.remote_state_bucket}"
//    key    = "${var.region}/${var.environment}/init/vpc_mgmt/terraform.tfstate"
//    region = "${var.region}"
//  }
//}

data "aws_region" "current" {}