
variable "remote_state_bucket_region" {
  description = "We will keep the RS in one region for now. In the future we could set cross region replication."
  default = "us-east-2"
}

variable "environment" {}

variable "region" {}

variable "remote_state_bucket" {}

variable "owner" {}

variable "account_id" {}

//TODO provide lookup for ami for region
variable "bastion_ami" {
  default = "ami-f63b1193"
}