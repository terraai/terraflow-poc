
variable "environment" {}

variable "region" {}

variable "owner" {}

variable "vpc_cidr" {}

variable "availability_zones" {
  type = "list"
}

variable "private_subnet_cidrs" {
  type = "list"
}

variable "public_subnet_cidrs" {
  type = "list"
}

//variable "database_subnets_cidrs" {
//  type = "list"
//}
//
//variable "redshift_subnets_cidrs" {
//  type = "list"
//}
//
//variable "elasticache_subnets_cidrs" {
//  type = "list"
//}
