variable "region" {}

variable "name" {}

variable "owner" {}

variable "environment" {}

variable "remote_state_bucket" {}