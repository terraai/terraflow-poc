provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {}
}

//resource "aws_kms_key" "this" {
//  deletion_window_in_days = 10
//}

resource "aws_s3_bucket" "this" {
  bucket = "terradrive-${var.owner}"

  //  server_side_encryption_configuration {
  //    rule {
  //      apply_server_side_encryption_by_default {
  //        kms_master_key_id = "${aws_kms_key.this.arn}"
  //        sse_algorithm = "aws:kms"
  //      }
  //    }
  //  }
}

resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id = "AllowExecutionFromS3Bucket"
  action = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.func1.arn}"
  principal = "s3.amazonaws.com"
  source_arn = "${aws_s3_bucket.this.arn}"
}

resource "aws_iam_policy" "lambda_logging" {
  name = "lambda_logging"
  path = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role = "${aws_iam_role.iam_for_lambda.name}"
  policy_arn = "${aws_iam_policy.lambda_logging.arn}"
}

resource "aws_cloudwatch_log_group" "lambda_cloudwatch_group" {
  name = "${var.name}"
}

//data "template_file" "cloudwatch_policy" {
//  template = "${file("policies/lambda-cloudwatch-role-policy.json")}"
//  vars {
//    name = "${var.name}"
//  }
//}
//
//resource "aws_iam_role_policy" "cloudwatch_policy" {
//  policy = "${data.template_file.cloudwatch_policy.rendered}"
//  role = "${aws_iam_role.iam_for_lambda.id}"
//}

//TODO include name of bucket in policy and include in s3_policy
//data "template_file" "cloudwatch_policy" {
//  template = "${file("policies/lambda-cloudwatch-role-policy.json")}"
//  vars {
//    name = "${var.name}"
//  }
//}

resource "aws_iam_role_policy" "s3_policy" {
  role = "${aws_iam_role.iam_for_lambda.id}"
  policy = "${file("policies/lambda-s3-role-policy.json")}"
}

resource "aws_iam_role_policy_attachment" "vpc_policy" {
  role = "${aws_iam_role.iam_for_lambda.id}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}

resource "aws_lambda_function" "func1" {
  filename = "lambda_function.zip"
  runtime = "python3.6"
  function_name = "${var.name}"
  role = "${aws_iam_role.iam_for_lambda.arn}"
  handler = "lambda_function.lambda_handler"

  vpc_config {
    security_group_ids = [
      "${data.terraform_remote_state.security_groups.database_sg_id}"]
    subnet_ids = [
      "${data.terraform_remote_state.vpc_main.database_subnets[0]}"]
  }
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "${aws_s3_bucket.this.id}"

  lambda_function {
    lambda_function_arn = "${aws_lambda_function.func1.arn}"
    events              = ["s3:ObjectCreated:*"]
//    filter_prefix       = "/"
    filter_suffix       = "/"
  }
}

data "terraform_remote_state" "security_groups" {
  backend = "s3"
  config {
    bucket = "${var.remote_state_bucket}"
    key = "${var.region}/${var.environment}/init/security_groups/terraform.tfstate"
    region = "${var.region}"
  }
}

data "terraform_remote_state" "vpc_main" {
  backend = "s3"
  config {
    bucket = "${var.remote_state_bucket}"
    key = "${var.region}/${var.environment}/init/vpc_main/terraform.tfstate"
    region = "${var.region}"
  }
}