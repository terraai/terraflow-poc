
provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {}
}

//resource "aws_key_pair" "cellspace_root_key" {
//  key_name = "cellspace_root_key"
//  public_key = "${file("ssh-keys/cellspace-root.pub")}"
//}

resource "aws_key_pair" "key_pair" {
  key_name = "testing"
  public_key = "${file("${path.module}/terraflow.pub")}"
}

//resource "aws_key_pair" "gpu_workers_key" {
//  key_name = "gpu_workers_key"
//  public_key = "${file("ssh-keys/cellspace-root.pub")}"
//}
//
//resource "aws_key_pair" "terraflow_key" {
//  key_name = "terraflow_key"
//  public_key = "${file("ssh-keys/cellspace-root.pub")}"
//}
//
//resource "aws_key_pair" "www_key" {
//  key_name = "www_key"
//  public_key = "${file("ssh-keys/cellspace-root.pub")}"
//}
//
//resource "aws_key_pair" "nfs_key" {
//  key_name = "nfs_key"
//  public_key = "${file("ssh-keys/cellspace-root.pub")}"
//}
