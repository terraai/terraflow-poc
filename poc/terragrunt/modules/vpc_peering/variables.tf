
variable "environment" {}

variable "region" {}

variable "owner" {}

variable "vpc_cidr" {}

variable "remote_state_bucket" {}

variable "availability_zones" {
  type = "list"
}

variable "private_subnet_cidrs" {
  type = "list"
}

variable "public_subnet_cidrs" {
  type = "list"
}
