
provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {}
}

//https://github.com/nicksantamaria/example-terraform-aws-vpc-peering/blob/master/main.tf

//resource "aws_vpc_peering_connection" "primary2secondary" {
//  peer_owner_id = "${data.aws_caller_identity.current.account_id}"
//  peer_vpc_id = "${aws_vpc.secondary.id}"
//  vpc_id = "${aws_vpc.primary.id}"
//  auto_accept = true
//}
//
//resource "aws_route" "primary2secondary" {
//  route_table_id = "${aws_vpc.primary.main_route_table_id}"
//  destination_cidr_block = "${aws_vpc.secondary.cidr_block}"
//  vpc_peering_connection_id = "${aws_vpc_peering_connection.primary2secondary.id}"
//}
//
//resource "aws_route" "secondary2primary" {
//  route_table_id = "${aws_vpc.secondary.main_route_table_id}"
//  destination_cidr_block = "${aws_vpc.primary.cidr_block}"
//  vpc_peering_connection_id = "${aws_vpc_peering_connection.primary2secondary.id}"
//}

resource "aws_vpc_peering_connection" "primary2secondary" {
  peer_owner_id = "${data.aws_caller_identity.current.account_id}"
  peer_vpc_id = "${aws_vpc.secondary.id}"
  vpc_id = "${aws_vpc.primary.id}"
  auto_accept = true
}

resource "aws_route" "primary2secondary" {
  route_table_id = "${aws_vpc.primary.main_route_table_id}"
  destination_cidr_block = "${aws_vpc.secondary.cidr_block}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.primary2secondary.id}"
}

resource "aws_route" "secondary2primary" {
  route_table_id = "${aws_vpc.secondary.main_route_table_id}"
  destination_cidr_block = "${aws_vpc.primary.cidr_block}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.primary2secondary.id}"
}



data "terraform_remote_state" "vpc-mgmt" {
  backend = "s3"
  config {
    bucket = "${var.remote_state_bucket}"
    key    = "${var.region}/${var.environment}/vpc-mgmt/terraform.tfstate"
    region = "${var.region}"
  }
}

data "terraform_remote_state" "vpc-master" {
  backend = "s3"
  config {
    bucket = "${var.remote_state_bucket}"
    key    = "${var.region}/${var.environment}/init/vpc-main/terraform.tfstate"
    region = "${var.region}"
  }
}
