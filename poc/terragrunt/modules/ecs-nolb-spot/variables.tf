
variable "region" {}

variable "name" {
  description = "Name to be used on all the resources as identifier"
  default = ""
}

variable "environment" {
  default = ""
}

variable "spot_price" {}

variable "aws_iam_role_name" {}

variable "remote_state_bucket" {}

variable "instance_tags" {
  description = "A map of tags to add to all resources"
  default = {}
}

variable "instance_count" {
  default = 1
}

variable "ami" {
  default = "ami-b86a5ddd"
}

variable "user_data_file" {}

variable "user_data_values" {
  default = {}
  type = "map"
}

variable "instance_type" {}

variable "iam_role_name" {
  default = ""
}

variable "subnet_id" {
  default = ""
}

variable "security_group_ids" {
  default = []
  type = "list"
}

variable "key_name" {
  default = ""
}

variable "volume_type" {
  default = "gp2"
}

variable "volume_size" {
  default = 20
}

variable "volume_tags" {
  default = {}
}

variable "task_file" {
  default = ""
}

variable "task_vars" {
  default = {}
}

variable "network_mode" {
  default = ""
}

variable "zone_id" {
  default = ""
}

variable "depends_on" {
  default = []
  type = "list"
}

variable "mount_target_id" {
  default = ""
}

variable "db_identifier" {
  default = ""
}