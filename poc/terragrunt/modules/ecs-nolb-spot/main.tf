//provider "aws" {
//  region = "${var.region}"
//}
//
//terraform {
//  backend "s3" {}
//}

locals {
  name = "${var.name}"
}

resource "aws_ecs_cluster" "this" {
  name = "${local.name}"
}

resource "aws_iam_instance_profile" "this" {
  name = "${local.name}-profile"
  path = "/"
  role = "${var.aws_iam_role_name}"
}

data "template_file" "this-user-data" {
  template = "${file("${var.user_data_file}")}"

  vars = "${var.user_data_values}"
  depends_on = [
    "aws_ecs_cluster.this"]
}

resource "aws_placement_group" "this" {
  name = "${local.name}"
  strategy = "cluster"
}

resource "aws_spot_instance_request" "this" {

  count = "${var.instance_count}"
  instance_type = "${var.instance_type}"
  spot_price = "${var.spot_price}"

  ami = "${var.ami}"
  vpc_security_group_ids = [
    "${var.security_group_ids}"]

  //  TODO Remove in prod
  key_name = "${var.key_name}"

  user_data = "${data.template_file.this-user-data.rendered}"

  iam_instance_profile = "${aws_iam_instance_profile.this.name}"

  //  TODO Distribute in multiple subnets https://github.com/hashicorp/terraform/issues/3234
  subnet_id = "${var.subnet_id}"

  root_block_device {
    volume_type = "standard"
    volume_size = "${var.volume_size}"
  }

  volume_tags {
    Name = "${local.name}"
    environment = "${var.environment}"
  }

  tags {
    Name = "${local.name}"
    environment = "${var.environment}"
  }

  depends_on = [
    "aws_ecs_cluster.this"
  ]

  lifecycle {
    ignore_changes = [
      "user_data"]
  }
}

data "aws_ecs_task_definition" "this" {
  task_definition = "${aws_ecs_task_definition.this.family}"
  depends_on = [
    "aws_ecs_task_definition.this"]
}

resource "aws_ecs_service" "this" {
  name = "${local.name}"
  cluster = "${aws_ecs_cluster.this.id}"
  task_definition = "${aws_ecs_task_definition.this.family}:${max("${aws_ecs_task_definition.this.revision}", "${data.aws_ecs_task_definition.this.revision}")}"
  desired_count = "${var.instance_count}"

  //  If going to use a load balancer, enable this role
  //  iam_role        = "${aws_iam_role.ecs_service_role.id}"

  ordered_placement_strategy = [
    {
      type = "spread"
      field = "instanceId"
    }
  ]

  placement_constraints {
    type = "distinctInstance"
  }

  depends_on = [
    "aws_ecs_cluster.this"]
}

data "template_file" "this-task" {
  template = "${file("${var.task_file}")}"

  vars = "${var.task_vars}"
}

# Simply specify the family to find the latest ACTIVE revision in that family.
resource "aws_ecs_task_definition" "this" {
  family = "${local.name}"

  container_definitions = "${data.template_file.this-task.rendered}"
  task_role_arn = "arn:aws:iam::830543758886:role/ecs-terraformer"

  volume {
    name = "dags"
    host_path = "/mnt/dags"
  }

  volume {
    name = "projects"
    host_path = "/mnt/projects"
  }

  network_mode = "host"
}

//resource "aws_route53_record" "this" {
//  count   = 1
//  zone_id = "${var.zone_id}"
//  name    = "${local.name}"
//  type    = "A"
//  ttl     = 60
//  records = ["${aws_eip.this.public_ip}"]
//}

