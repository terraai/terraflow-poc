provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {}
}

resource "aws_key_pair" "key_pair" {
  public_key = "${file("${path.module}/terraflow.pub")}"
}

module "logging" {
  source = "../logging_ecs"
  region = "${var.region}"
  cloudwatch_prefix = "${var.cloudwatch_prefix}"
}

module "iam" {
  source = "../iam"
  name = "${var.name}_cpu_tmp"
  environment = "${var.environment}"
  region = "${var.region}"
  role = "${path.module}/roles/ecs-instance-role.json"
  policy = "${path.module}/policies/ecs-instance-role-policy.json"
}

module "terraflow_cpu_tmp" {
  source = "../ecs-nolb-spot"
  remote_state_bucket = "${var.remote_state_bucket}"
  name = "${var.environment}_${var.name}_cpu"
  region = "${var.region}"
  environment = "${var.environment}"

  instance_count = "${var.instance_count}"
  instance_type = "${var.instance_type}"
  spot_price = "${var.spot_price}"

  subnet_id = "${data.terraform_remote_state.vpc_main.public_subnets[0]}"

  security_group_ids = [
    "${data.terraform_remote_state.security_groups.www_sg_id}"]

  aws_iam_role_name = "${module.iam.aws_iam_role_name}"

  key_name = "${aws_key_pair.key_pair.key_name}"

  ami = "${var.ami}"
  volume_size = 8

  user_data_file = "${path.module}/user-data/terraflow.sh"

  user_data_values = {
    environment = "${var.environment}"
    ecs_config = "echo '' > /etc/ecs/ecs.config"
    ecs_logging = "[\"json-file\",\"awslogs\"]"
    //    MUST BE SAME AS name ^^^
    cluster_name = "${var.environment}_${var.name}_cpu"
    cloudwatch_prefix = "${var.cloudwatch_prefix}"
  }

  task_file = "${path.module}/task-definitions/terraflow-cpu.json"
  task_vars = {
    queue = "${var.queue}"

    service_name = "${var.name}"
    account_id = "${var.account_id}"
    region = "${var.region}"
    image = "terraflow-opm"
    environment = "${var.environment}"
    fernet_key = "kZji0e9Mf28paWhX6KRu-tPPECh1TVgUXu_AYkpN0XA="
    rds_address = "${data.terraform_remote_state.rds.rds_address}"
    rds_database = "${var.database}"
    rds_username = "${var.username}"
    rds_password = "${var.password}"
    rds_host = "${data.terraform_remote_state.rds.rds_address}"
    redis_host = "${data.terraform_remote_state.terraflow.redis_private_ip}"
  }
}

data "terraform_remote_state" "rds" {
  backend = "s3"
  config {
    bucket = "${var.remote_state_bucket}"
    key = "${var.region}/${var.environment}/init/rds/terraform.tfstate"
    region = "${var.region}"
  }
}

data "terraform_remote_state" "iam" {
  backend = "s3"
  config {
    bucket = "${var.remote_state_bucket}"
    key = "${var.region}/${var.environment}/services/terraflow/iam/terraform.tfstate"
    region = "${var.region}"
  }
}

data "terraform_remote_state" "security_groups" {
  backend = "s3"
  config {
    bucket = "${var.remote_state_bucket}"
    key = "${var.region}/${var.environment}/init/security_groups/terraform.tfstate"
    region = "${var.region}"
  }
}

data "terraform_remote_state" "vpc_main" {
  backend = "s3"
  config {
    bucket = "${var.remote_state_bucket}"
    key = "${var.region}/${var.environment}/init/vpc_main/terraform.tfstate"
    region = "${var.region}"
  }
}

data "terraform_remote_state" "terraflow" {
  backend = "s3"
  config {
    bucket = "${var.remote_state_bucket}"
    key = "${var.region}/${var.environment}/services/terraflow/terraform.tfstate"
    region = "${var.region}"
  }
}
