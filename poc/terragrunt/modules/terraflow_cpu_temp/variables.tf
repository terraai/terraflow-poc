

// Main variables for template
variable "queue" {}

variable "instance_count" {}

variable "instance_type" {}

variable "spot_price" {}

variable "name" {}

// Inherited variables

variable "account_id" {}

variable "region" {}

variable "environment" {}

variable "remote_state_bucket" {}

variable "cloudwatch_prefix" {}

variable "zone_id" {}

// Standard variables
variable "ami" {
  default = "ami-956e52f0"
}

variable "volume_size" {
  default = 8
}

variable "database" {}
variable "username" {}
variable "password" {}

