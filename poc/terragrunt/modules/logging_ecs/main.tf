
provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {}
}

resource "aws_cloudwatch_log_group" "this_dmesg" {
  name              = "${var.cloudwatch_prefix}/var/log/dmesg"
  retention_in_days = 30
}

resource "aws_cloudwatch_log_group" "this_docker" {
  name              = "${var.cloudwatch_prefix}/var/log/docker"
  retention_in_days = 30
}

resource "aws_cloudwatch_log_group" "this_agent" {
  name              = "${var.cloudwatch_prefix}/var/log/ecs/ecs-agent.log"
  retention_in_days = 30
}

resource "aws_cloudwatch_log_group" "this_init" {
  name              = "${var.cloudwatch_prefix}/var/log/ecs/ecs-init.log"
  retention_in_days = 30
}

resource "aws_cloudwatch_log_group" "this_audit" {
  name              = "${var.cloudwatch_prefix}/var/log/ecs/audit.log"
  retention_in_days = 30
}

resource "aws_cloudwatch_log_group" "this_messages" {
  name              = "${var.cloudwatch_prefix}/var/log/messages"
  retention_in_days = 30
}
