
variable "name" {}

variable "account_id" {}

variable "region" {}

variable "environment" {}

variable "remote_state_bucket" {}

variable "cloudwatch_prefix" {}

variable "ami" {
  default = "ami-956e52f0"
}

//variable "aws_iam_role_name" {}

variable "volume_size" {}

variable "database" {}
variable "username" {}
variable "password" {}

variable "zone_id" {}