provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {}
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "1.34.0"

  name = "${var.environment}"
  cidr = "${var.vpc_cidr}"

  azs             = ["${var.availability_zones}"]

  public_subnets  = ["${var.public_subnet_cidrs}"]
  private_subnets = ["${var.private_subnet_cidrs}"]
  database_subnets = ["${var.database_subnets_cidrs}"]
  redshift_subnets = ["${var.redshift_subnets_cidrs}"]
  elasticache_subnets = ["${var.elasticache_subnets_cidrs}"]

  enable_nat_gateway = true
  enable_dns_support = true
  enable_dns_hostnames = true

//  While in testing, keep this as one until we need the bandwidth.  Don't know impact it has on routing but assuming ok.
  single_nat_gateway = true

  tags = {
    Terraform = "true"
    Environment = "${var.environment}"
    Owner = "${var.owner}"
  }
}

