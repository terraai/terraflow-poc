

output "vpc_id" {
  value = "${module.vpc.vpc_id}"
}

output "nat_public_ips" {
  value = "${module.vpc.nat_public_ips}"
}

output "private_subnets" {
  description = "List of IDs of private subnets"
  value       = ["${module.vpc.private_subnets}"]
}

output "private_subnets_cidr_blocks" {
  description = "List of cidr_blocks of private subnets"
  value       = ["${module.vpc.private_subnets_cidr_blocks}"]
}

output "public_subnets" {
  description = "List of IDs of public subnets"
  value       = ["${module.vpc.public_subnets}"]
}

output "public_subnets_cidr_blocks" {
  description = "List of cidr_blocks of public subnets"
  value       = ["${module.vpc.public_subnets_cidr_blocks}"]
}

output "database_subnets" {
  description = "List of IDs of database subnets"
  value       = ["${module.vpc.database_subnets}"]
}

output "database_subnets_cidr_blocks" {
  description = "List of cidr_blocks of database subnets"
  value       = ["${module.vpc.database_subnets_cidr_blocks}"]
}

output "database_subnet_group" {
  description = "ID of database subnet group"
  value       = "${module.vpc.database_subnet_group}"
}

output "redshift_subnets" {
  description = "List of IDs of redshift subnets"
  value       = ["${module.vpc.redshift_subnets}"]
}

output "redshift_subnets_cidr_blocks" {
  description = "List of cidr_blocks of redshift subnets"
  value       = ["${module.vpc.redshift_subnets_cidr_blocks}"]
}

output "redshift_subnet_group" {
  description = "ID of redshift subnet group"
  value       = "${module.vpc.redshift_subnet_group}"
}

output "elasticache_subnets" {
  description = "List of IDs of elasticache subnets"
  value       = ["${module.vpc.elasticache_subnets}"]
}

output "elasticache_subnets_cidr_blocks" {
  description = "List of cidr_blocks of elasticache subnets"
  value       = ["${module.vpc.elasticache_subnets_cidr_blocks}"]
}

output "intra_subnets" {
  description = "List of IDs of intra subnets"
  value       = ["${module.vpc.intra_subnets}"]
}

output "intra_subnets_cidr_blocks" {
  description = "List of cidr_blocks of intra subnets"
  value       = ["${module.vpc.intra_subnets_cidr_blocks}"]
}

output "elasticache_subnet_group" {
  description = "ID of elasticache subnet group"
  value       = "${module.vpc.elasticache_subnet_group}"
}

output "elasticache_subnet_group_name" {
  description = "Name of elasticache subnet group"
  value       = "${module.vpc.elasticache_subnet_group_name}"
}

