import os

import boto3
import botocore
import zipfile


def initialize_project_structure(cfg):
    """
    Initializes the model directory
    :param cfg:
    :return:
    """
    print('Initializing project structure')
    bucket = cfg.model.bucket
    key = cfg.model.key
    file_name = cfg.model.key.split('/')[-1]

    s3 = boto3.resource('s3')

    base_dir = '/'.join([cfg.project.directory, cfg.project.name, cfg.model.name])
    os.system('mkdir -p ' + '/'.join([base_dir, 'Inputs']))
    os.system('mkdir -p ' + '/'.join([base_dir, 'Runs', cfg.run.id]))

    if cfg.run.iterations.current == 1:
        destination = base_dir + 'Inputs/'
    else:
        destination = base_dir + 'Runs/' + cfg.run.id + '/'

    try:
        s3.Bucket(bucket).download_file(key, destination + file_name)
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("The object does not exist.")
        else:
            raise

    print('Downloaded \nBucket=' + bucket + ' \nKey=' + key + ' \nFile name=' + file_name + ' \nDestination=' + destination)

    os.system('unzip -o -d ' + destination + ' ' + destination + file_name)
    os.system('cp -r ' + destination + '/' + 'ECLIPSE/* ' + destination + '/')

# def initialize_project_structure(cfg):
#     """
#     Initializes the model directory
#     :param cfg:
#     :return:
#     """
#     print('Initializing project structure')
#     base_dir = cfg.project.directory + cfg.project.name + '/' + cfg.model.name + '/'
#     os.system('mkdir -p ' + base_dir + 'Inputs')
#     os.system('mkdir -p ' + base_dir + 'Runs/' + cfg.run.id)

    # if cfg.run.iterations.current == 1:
    #     os.system('aws s3 cp s3://' + cfg.model.bucket + '/' + cfg.model.key + ' ' + base_dir + 'Inputs/')
    #     os.system('cd ' + base_dir + 'Inputs/')
    #     os.system('unzip ' + cfg.model.key.split('/')[-1])
    #     os.system('rm ' + base_dir + 'Inputs/' + cfg.model.key.split('/')[-1])
    #     os.system('cp -r ' + base_dir + 'Inputs/' + 'ECLIPSE/* ' + base_dir + 'Inputs/')
    # else:
    #     os.system('aws s3 cp s3://' + cfg.model.bucket + '/' + cfg.model.key + ' ' + base_dir + 'Runs/' + cfg.run.id + '/')
    #     os.system('cd ' + base_dir + 'Runs/' + cfg.run.id + '/')
    #     os.system('unzip ' + base_dir + 'Runs/' + cfg.run.id + '/' + cfg.model.key.split('/')[-1])
    #     os.system('rm ' + base_dir + 'Runs/' + cfg.run.id + '/' + cfg.model.key.split('/')[-1])
    #     os.system('cp -r ' + base_dir + 'Runs/' + cfg.run.id + '/' + 'ECLIPSE/* ' + base_dir + 'Runs/' + cfg.run.id + '/')

def model_directory(cfg, i, r):
    """
    Returns the absolute path of the model directory
    :param cfg:
    :param i:
    :param r:
    :return:
    """

    directory = ''
    base_dir = cfg.project.directory + cfg.project.name + '/' + cfg.model.name + '/'
    if cfg.model.location == 'local':

        if i == cfg.run.iterations.current and cfg.model.input[:6] == 'Inputs':
            directory = base_dir + cfg.model.input + cfg.model.prefix + '_' + str(r) + '/'

        if i == cfg.run.iterations.current and cfg.model.input[:4] == 'Runs':
            directory = base_dir + cfg.model.input + cfg.model.prefix + '_' + str(r) + '_' + str(i) + '/'

        if i != cfg.run.iterations.current:
            directory = base_dir + 'Runs/' + cfg.run.id + '/' + cfg.model.prefix + '_' + str(r) + '_' + str(i) + '/'

    if cfg.model.location == 's3':

        if i == 1:

            os.system('aws s3 cp s3://' + cfg.model.bucket + '/' + cfg.model.key + ' /' + base_dir + '/Inputs/')
            os.system('unzip ' + '/' + base_dir + '/Inputs/')

        else:
            os.system('aws s3 cp s3://' + cfg.model.bucket + '/Runs/' + cfg.model.prefix + '-' + str(r) + '-' + str(i) + '/'
                      + str(r) + '-' + str(i) + '/')

    return directory


def create_new_iter_dir(cfg, i):

    base_dir = cfg.project.directory + cfg.project.name + '/' + cfg.model.name + '/Runs/' + cfg.run_id

    # Make run dir if not there
    if not os.path.isdir(base_dir + 'Runs/' + cfg.run.id):
        os.mkdir(base_dir + 'Runs/' + cfg.run.id)




