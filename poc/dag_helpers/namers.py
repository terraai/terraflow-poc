

def sim_task_name(cfg, i, r):
    if (cfg.models.naming_convention is None) or (cfg.models.naming_convention == 'default'):
        return 'iter_' + str(i) + '-real_' + str(r)
    else:
        raise ValueError('Unrecognized naming convention')


def agg_task_name(cfg, i):
    if (cfg.models.naming_convention is None) or (cfg.models.naming_convention == 'default'):
        return 'iter_' + str(i)
    else:
        raise ValueError('Unrecognized naming convention')


def model_namer(cfg, i, r):

    model_name = ''
    if cfg.simulator.name in ['opm_flow', 'eclipse']:
        if i == 1:
            model_name = 'OLYMPUS_' + str(r) + '.DATA'
        else:
            model_name = 'OLYMPUS_' + str(r) + '_' + str(i) + '.DATA'

    return model_name

