
import json
from collections import namedtuple


class Config(object):

    def __init__(self, cfg_path):
        self.cfg_path = cfg_path

    def load(self):
        with open (self.cfg_path) as json_data:
            self.cfg = json.load(json_data, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
        return self.cfg

