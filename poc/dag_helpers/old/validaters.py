import os

from dag_helpers.directories import model_directory

def check_all_sims_exist(cfg, i):


    for r in range(1, cfg.realization['max'] +1):
        if not os.path.exists(model_directory(cfg, i, r)):
            raise erro