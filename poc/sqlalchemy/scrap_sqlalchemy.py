from sqlalchemy import (MetaData, Table, Column, Integer, String, ForeignKey, Date, Boolean,
                        create_engine)
from sqlalchemy.orm import polymorphic_union
from sqlalchemy.orm import (mapper, relationship, sessionmaker)

schema = 'terraflow'
meta = MetaData(schema=schema)

project_table = Table('projects', meta,
                      Column('project_id', Integer, primary_key=True),
                      Column('project_name', String(20), nullable=False, key='project_name'),
                      Column('s3_bucket', String(20), key='s3_bucket'),
                      schema=schema
                      )

model_table = Table('models', meta,
                    Column('model_id', Integer, primary_key=True),
                    Column('project_id', Integer, ForeignKey('projects.project_id', ondelete="CASCADE")),
                    Column('name', String(50), nullable=False, key='model.name'),
                    Column('model_type', String, nullable=False, key='model_type'),
                    schema=schema)

pjoin = polymorphic_union({
    'projects': project_table,
    'models': model_table,
}, 'type', 'pjoin')


class Project(object):

    def __init__(self):
        # self.project_id = 1
        self.project_name = 'pr'
        self.s3_bucket = 's3_buck'


class Model(Project):
    def __init__(self):
        super(Model, self).__init__()
        # self.model_id = 2
        self.project_id = 3
        self.name = 'lkjs'
        self.model_type = 'flow'


project_mapper = mapper(Project,
                        pjoin,
                        with_polymorphic=('*', pjoin),
                        polymorphic_on=pjoin.c.type)

model_mapper = mapper(Model,
                      model_table,
                      inherits=project_mapper,
                      concrete=True,
                      polymorphic_identity='models')



if __name__ == '__main__':
    url = 'postgres+psycopg2://terra:fundus500k@fetl-ds.c4lstuf6msdr.us-west-2.rds.amazonaws.com:5432/metl'
    engine = create_engine(url, echo=True)
    meta.create_all(engine)

    # expire_on_commit=False means the session contents
    # will not get invalidated after commit.
    sess = sessionmaker(engine, expire_on_commit=False)()

    prj = Project()
    mod = Model()

    sess.add(prj)
    sess.add(mod)
    sess.commit()
    sess.close()
