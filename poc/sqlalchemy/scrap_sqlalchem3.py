from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, create_engine, MetaData
from sqlalchemy.orm import sessionmaker, relationship

schema = 'terraflow'

meta = MetaData(schema=schema)

Base = declarative_base()


class Project(Base):
    # def __init__(self):
    #     self.name = None

    __tablename__ = 'projects_test'
    id = Column(Integer, primary_key=True)
    name = Column(String)

    @classmethod
    def persist(cls):
        url = 'postgres+psycopg2://terra:fundus500k@fetl-ds.c4lstuf6msdr.us-west-2.rds.amazonaws.com:5432/metl'
        engine = create_engine(url, echo=True)
        Base.metadata.create_all(engine)
        sess = sessionmaker(engine, expire_on_commit=False)()
        sess.add(cls)
        sess.commit()
        sess.close()


class Model(Base):
    __tablename__ = 'models_test'
    id = Column(Integer, primary_key=True)
    project_id = (Integer, ForeignKey('project.pid'))
    model_name = Column(String)


if __name__ == '__main__':

    prj = Project(name='what???')
    prj.persist()

    # url = 'postgres+psycopg2://terra:fundus500k@fetl-ds.c4lstuf6msdr.us-west-2.rds.amazonaws.com:5432/metl'
    # engine = create_engine(url, echo=True)
    # Base.metadata.create_all(engine)
    #
    # # expire_on_commit=False means the session contents
    # # will not get invalidated after commit.
    # sess = sessionmaker(engine, expire_on_commit=False)()
    #
    # prj = Project(name='sdvx')
    # mod = Model(model_name='duh')
    #
    # sess.add(prj)
    # sess.add(mod)
    # sess.commit()
    # sess.close()