# from sqlalchemy import (MetaData, Table, Column, Integer, String, ForeignKey, Date, Boolean,
#                         create_engine)
# from sqlalchemy.orm import (mapper, relationship, sessionmaker)
# from datetime import datetime
#
# meta = MetaData(schema="test")
# schema = 'test'
#
# project_table = Table('projects', meta,
#                       Column('id', Integer, primary_key=True),
#                       Column('project_name', String(20), nullable=False, key='name'),
#                       Column('s3_bucket')
#                       )
#
# model_table = Table('models', meta,
#                     Column('id', Integer, primary_key=True),
#                     Column('name', String(50), nullable=False, key='name'),
#                     Column('model_type', String, nullable=False, key='model_type'),
#                     Column('last_run_date', Date, key='last_run_date'),
#                     schema=schema)
#
# run_table = Table('runs', meta,
#                   Column('id', Integer, primary_key=True),
#                   Column('name', String(50), nullable=False, key='name'),
#                   Column('running', Boolean, key='running'),
#                   Column('real_id', Integer, ForeignKey('reals.id', ondelete="CASCADE")),
#                   Column('config', String, key='config'),
#                   schema=schema)
#
# real_table = Table('reals', meta,
#                    Column('id', Integer, primary_key=True),
#                    Column('real_num', Integer, nullable=False, key='real_num'),
#                    Column('running', Boolean, key='running'),
#                    Column('model_id', Integer, ForeignKey('models.id', ondelete="CASCADE")),
#                    Column('params', String, key='params'),
#                    schema=schema)
#
# iter_table = Table('iters', meta,
#                    Column('id', Integer, primary_key=True),
#                    Column('iter_num', Integer, nullable=False, key='iter_num'),
#                    Column('running', Boolean, key='running'),
#                    Column('run_id', Integer, ForeignKey('runs.id', ondelete="CASCADE")),
#                    Column('data', String, key='data'),
#                    schema=schema)


class Project(object):

    def __init__(self, source=None):
        from os import getcwd
        if source is None:
            self.config = None
            self.name = None
            self.project_name = None
            self.local_directory = getcwd()
            self.backend = None
            self.backend_provider = None
            self.backend_name = None
            self.version = None

        else:
            self.import_config(source)

    def create_new(self, backend):
        self.backend = backend

        if backend == 'local':
            self.initialize_local_directory()
        elif backend == 's3':
            self.initialize_s3_obj_store()

    def import_config(self,
                      source,
                      dbname=None,
                      user=None,
                      host=None,
                      password=None):

        config = {}
        if source.split('.')[-1] == 'json':
            config = self.json_import(source)

        if source == 'postgres':
            config = self.postgres_import(dbname, user, host, password)

        self.config = config
        self.backend = self.config['project']['backend']
        self.backend_provider = self.config['project']['backend']['provider']
        self.backend_name = self.config['project']['backend']['name']
        self.version = config['version']
        self.project_name = config['project']['name']
        self.name = config['project']['name']

    def json_import(self, cfg_path):
        from json import load
        with open(cfg_path) as json_data:
            self.config = load(json_data)
        return self.config

    def postgres_import(self, dbname, user, host, password):
        import psycopg2

        try:
            connect_str = "dbname=%s user=%s host=%s password=%s" % (dbname, user, host, password)
            conn = psycopg2.connect(connect_str)
            cursor = conn.cursor()
            cursor.execute("""SELECT config from project where project_name=%s""" % self.project_name)
            self.config = cursor.fetchall()

        except Exception as e:
            print("Uh oh, can't connect. Invalid dbname, user or password?")
            print(e)
        return self.config

    def initialize_local_directory(self):
        from os import system
        system('mkdir -p ' + '/'.join([self.local_directory, self.name, 'Sources']))

    def initialize_s3_obj_store(self, bucket='terraai-projects'):
        import boto3
        client = boto3.client('s3')

        client.put_object(
            Body='',
            Bucket=bucket,
            Key='/'.join([self.project_name]) + '/'
        )

    def import_from_s3(self,
                       destination='.',
                       bucket='terraai-projects'):
        from os import system
        system(' '.join(['aws s3 cp s3://' + '/'.join([bucket, self.project_name]), destination, '--recursive']))

    def import_source(self,
                      destination='.',
                      bucket='terraai-projects'):
        from os import system
        system(' '.join(['aws s3 cp s3://' + '/'.join([bucket, self.project_name]), destination, '--recursive']))

    def upload_to_s3(self,
                     dir=None,
                     bucket='terraai-projects',
                     zip=False
                     ):
        import os

        if not dir:
            project_dir = '/'.join([self.dir, self.project_name])

        if os.path.exists(project_dir):
            models = os.listdir(project_dir)
            models = list(filter(lambda a: a[:1] != '.', models))
            if zip:
                raise NotImplementedError
            else:
                for m in models:
                    os.system(' '.join(['aws s3 cp ', '/'.join([project_dir, m]),
                                        's3://' + '/'.join([bucket, self.project_name]), '--recursive']))

    # def create_new(self):
    #     if self.backend =


class Model(Project):

    def __init__(self, source=None):
        if source is None:
            self.name = None
            super(Model, self).__init__()
        elif source.split('.')[-1] == 'json':
            self.import_config(source)

    def import_config(self, source,
                      dbname=None,
                      user=None,
                      host=None,
                      password=None):
        super(Model, self).import_config(source)

    # def __getattr__(self, item):
    #     return getattr(self.project, item)


class Run(Project):

    def __init__(self):
        super(Run, self).__init__()


# mapper(Project, model_table, properties={
#     'reals': relationship(Model,
#                           lazy="dynamic",
#                           cascade="all, delete-orphan",
#                           passive_deletes=True,
#                           )
# }
#        )
#
# mapper(Model, model_table, properties={
#     'reals': relationship(Project,
#                           lazy="dynamic",
#                           cascade="all, delete-orphan",
#                           passive_deletes=True,
#                           )
# }
#        )
#
# mapper(Run, real_table, properties={
#     'runs': relationship(Model,
#                          lazy="dynamic",
#                          cascade="all, delete-orphan",
#                          passive_deletes=True,
#                          )
# }
#        )
#
# mapper(Run, run_table, properties={
#     'iters': relationship(Iter,
#                           lazy="dynamic",
#                           cascade="all, delete-orphan",
#                           passive_deletes=True,
#                           )
# }
#        )
#




if __name__ == '__main__':
    # cfg_path = 'scrap.json'
    # prj = Project()
    # prj.
    # print(prj.version)
    mod = Model()

    print(mod.name)
    # mod.create_new()
    # mod.import_config(source='postgres')

    print(mod.project_name)

    # mod.import_config(cfg_path)
    print(mod.version)

    # mod.version = 'stjlj'
    # print(mod.version)
    # mod.import_config(cfg_path)
    # print(prj.version)
