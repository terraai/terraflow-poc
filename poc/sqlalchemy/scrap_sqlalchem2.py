from sqlalchemy import (MetaData, Table, Column, Integer, String, ForeignKey, Date, Boolean,
                        create_engine)
from sqlalchemy.orm import (mapper, relationship, sessionmaker)
from sqlalchemy.orm import polymorphic_union

pjoin = polymorphic_union({
    'employee': employees_table,
    'manager': managers_table,
    'engineer': engineers_table
}, 'type', 'pjoin')

schema = 'terraflow'
meta = MetaData(schema=schema)

project_table = Table('projects', meta,
                      Column('id', Integer, primary_key=True),
                      Column('project_name', String(20), nullable=False, key='name'),
                      Column('s3_bucket', String(20), key='s3_bucket'),
                      schema=schema
                      )

model_table = Table('models', meta,
                    Column('id', Integer, primary_key=True),
                    Column('project_id', Integer, ForeignKey('projects.id', ondelete="CASCADE")),
                    Column('name', String(50), nullable=False, key='name'),
                    Column('model_type', String, nullable=False, key='model_type'),
                    schema=schema)


class Project(object):

    __tablename__ = 'projects'
    Column('id', Integer, primary_key=True)
    Column('project_name', String(20), nullable=False, key='name')
    Column('s3_bucket', String(20), key='s3_bucket')

    def __init__(self):
        self.id = 1
        self.project_name = 'pr'
        self.s3_bucket = 's3_bucket a'


class Model(Project):
    __tablename__ = 'models'
    id = Column('id', Integer, primary_key=True)
    project_id = Column('project_id', Integer, ForeignKey('projects.id', ondelete="CASCADE"))
    name = Column('name', String(50), nullable=False, key='name')
    model_type = Column('model_type', String, nullable=False, key='model_type')



    def __init__(self):
        self.id = 1
        self.name = 'lkjs'
        self.model_type = 'flow'


mapper(Project, model_table, properties={
    'models': relationship(Model,
                          lazy="dynamic",
                          cascade="all, delete-orphan",
                          passive_deletes=True,
                          )
}
       )

mapper(Model, model_table)


if __name__ == '__main__':
    url = 'postgres+psycopg2://terra:fundus500k@fetl-ds.c4lstuf6msdr.us-west-2.rds.amazonaws.com:5432/metl'
    engine = create_engine(url, echo=True)
    meta.create_all(engine)

    # expire_on_commit=False means the session contents
    # will not get invalidated after commit.
    sess = sessionmaker(engine, expire_on_commit=False)()

    prj = Project
    mod = Model()

    sess.add(prj)
    sess.add(mod)
    sess.commit()
    sess.close()