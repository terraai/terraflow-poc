


class Adder(object):
    def __init__(self, first, second):
        self.first = first
        self.second = second

    def add(self):
        return self.first + self.second

if __name__ == '__main__':
    a = Adder(1, 2)
    print(a.add())
    
