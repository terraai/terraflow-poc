import unittest
from poc.testing.bin.app import *

class AdderTest(unittest.TestCase):

    def test_addition(self):
        a = Adder(1, 2)
        self.assertEqual(4, a.add())