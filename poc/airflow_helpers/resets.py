

import os


def restart_airflow(webserver_port=None):
    print "Restarting airflow"
    os.system('cat $AIRFLOW_HOME/airflow-webserver.pid | xargs kill -9')

    if webserver_port is None:
        print "Using default port 8080"
        os.system('airflow webserver -p ' + webserver_port)
    else:
        os.system('airflow webserver -p 8080')


def reset_dag(dag_id):
    print "Reseting dag " + dag_id
    os.system('rm $AIRFLOW_HOME/dags/' + dag_id + '.pyc')


def rerun_dag(dag_id):
    print "Rerunning dag = " + dag_id
    os.system('airflow backfill -s ')