#!/bin/bash
# Get main dags folder to
mkdir /mnt/efs/efs_dags
aws s3 cp s3://terraflow/dags.zip /mnt/efs/efs_dags.zip
unzip /mnt/efs/efs_dags.zip
ln -s /mnt/efs/efs_dags $AIRFLOW_HOME/dags

# Look through array of dependencies
declare -a folders=("dag_helpers" "sims" "updates" "airflow_helpers")
for i in "${folders[@]}"
do
    aws s3 cp s3://terraflow/"$i".zip /mnt/efs/efs_dags/"$i".zip
    unzip "$i".zip "$i"
done

# Reset airflow
airflow initdb