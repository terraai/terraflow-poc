# Copyright (C) terra.ai Inc. - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Reza Khaninezhad and Rob Cannon and <rob@terra.ai> 2018

from dag_helpers.directories import model_directory


def calibrate_model(cfg, i):
    print("Calibration step")

    for r in range(1, cfg.realizations.max + 1):
        model_dir = model_directory(cfg, i, r)
        print(model_dir)

