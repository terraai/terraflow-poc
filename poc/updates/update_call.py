# Copyright (C) terra.ai Inc. - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Reza Khaninezhad and Rob Cannon and <rob@terra.ai> 2018

from updates.calibration import calibrate_model
from updates.optimization import wp_optimization


def update_call(cfg, i):

    if cfg.update['type'] == 'calibration':
        print("Calibrating model")
        calibrate_model(cfg, i)

    if cfg.update['type'] == 'wp_optimization':
        print("Optimizing model")
        wp_optimization(cfg, i)
