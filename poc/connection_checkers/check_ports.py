import socket
from contextlib import closing

target_ip = '18.188.171.163'

ports = [80, 81, 6767, 6066, 7077, 2049]

def check_socket(target_ip, ports):
    for p in ports:
        with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
            if sock.connect_ex((target_ip, p)) == 0:
                print "Port %s is open" % p
            else:
                print "Port %s is not open" % p

check_socket(target_ip, ports)
