import psycopg2
import argparse


# default_upd = 'terraflow'
#
# parser = argparse.ArgumentParser(description='Check connection to postgres server')
# parser.add_argument('--server', required=True, help='Define pstgres server')
# parser.add_argument('--database', default=default_upd, help='Define database (default: %(default)s)')
# parser.add_argument('--port', type=int, default=5672, help='Define port (default: %(default)s)')
# parser.add_argument('--username', default=default_upd, help='Define username (default: %(default)s)')
# parser.add_argument('--password', default=default_upd, help='Define password (default: %(default)s)')
# args = vars(parser.parse_args())

# postgres+psycopg2://terraflow:terraflow@terraflowrds.ccdtpkr2drkq.us-east-2.rds.amazonaws.com:5432/terraflowRds

def test_conn(host, dbname, user, password):
    # Define our connection string
    conn_string = "host='%s' dbname='%s' user='%s' password='%s'"%(host, dbname, user, password)

    # print the connection string we will use to connect
    print "Connecting to database\n	->%s" % (conn_string)

    # get a connection, if a connect cannot be made an exception will be raised here
    conn = psycopg2.connect(conn_string)

    # conn.cursor will return a cursor object, you can use this cursor to perform queries
    cursor = conn.cursor()
    print "Connected!\n"


if __name__ == "__main__":
    host = 'terraflowrds.ccdtpkr2drkq.us-east-2.rds.amazonaws.com'
    dbname = 'terraflowRds'
    user = 'terraflow'
    password = 'terraflow'

    # host = 'fetl-ds.c4lstuf6msdr.us-west-2.rds.amazonaws.com'
    # dbname = 'metl'
    # user = 'terra'
    # password = 'fundus500k'
    test_conn(host, dbname, user, password)