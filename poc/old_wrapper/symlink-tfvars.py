"""
In order to keep all the scripts DRY, all the deployment parts need a sym link to a centralized tfvars file.
This script builds those links. It is needed because in windows, symlinks are different than in linux. Symlinks
also don't play nice in git. Hence the script...
"""
import os
from pathlib import Path

tfvar = 'terraform.tfvars'
folder_list = ['vpc', 'services/terraflow-ws']

for folder in folder_list:

    tfvar_path = Path(os.path.join(Path(__file__).parents[1], tfvar))
    sym_path = Path(os.path.join(Path(__file__).parents[1], folder, tfvar))
    if sym_path.is_symlink():
        print('Rebuilding symlink = ' + str(sym_path))
        sym_path.unlink()
        sym_path.symlink_to(tfvar_path)
    else:
        print('Making new sym link ' + str(sym_path))
        sym_path.symlink_to(tfvar_path)
