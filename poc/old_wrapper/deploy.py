#!/usr/bin/env python
# -*- coding: utf-8 -*-
from python_terraform import *
from pathlib import Path
import argparse


class Stack(object):
    def __init__(self):
        pass

class Component(object):
    def __init__(self, name):
        self.name = name

    def create_from_template(self):


    @property
    def dir(self):
        return 'emring'


class Action(object):
    def __init__(self,
                 tfvar_path = '../terraform.tfvars'):
        self.tfvar_path = tfvar_path

    def init(self, component_name):
        comp = Component(component_name)
        if os.path.exists(str(dir)):
            tf = Terraform(working_dir=str(comp))

    def apply(self):
        pass

    def destroy(self):

        pass


    def _get_component_path(self):
        pass


    def _get_everything(self,
                         inventory_file='inventory.yml'):
        from ruamel import yaml

        inventory_dict = {}
        with open(inventory_file, 'r') as file:
            try:
                inventory_dict = yaml.safe_load(file)
            except yaml.YAMLError as exc:
                print('Formatting error with yml')
                print(exc)

        inventory_path_list = []
        for item in inventory_dict.keys():
            print(item)
            for comp in inventory_dict[item]:
                inventory_path_list.append(Path(os.path.join(Path(__file__).parents[1], '/'.join([item, comp]))))

        return inventory_path_list

    def __sleeper_fun(self, sleep_time):
        from sys import platform
        if platform.lower()[:3] == 'win':
            sleep_fun = 'ping 127.0.0.1 -n %s > nul' % sleep_time
        else:
            sleep_fun = 'sleep ' % sleep_time
        return sleep_fun


def sleeper(sleep_time):
    """
    Multi-platform sleeping for iam role instantiation and local exec
    :param sleep_time: time in seconds to sleep
    :return:
    """
    from sys import platform
    if platform.lower()[:3] == 'win':
        sleep_fun = 'ping 127.0.0.1 -n %s > nul' % sleep_time
    else:
        sleep_fun = 'sleep ' % sleep_time
    return sleep_fun


def prompt(action, resources):
    """
    Checks user input for proceeding
    :param check_type:
    :return:
    """
    while True:
        try:
            check_destroy = str(input("Are you sure you want to %s %s?" % (action, resources)))
        except ValueError:
            print('Not string, try again')
            continue
        if check_destroy in ['yes', 'y', 'Yes', 'yup']:
            break
        else:
            print("Not valid entry for proceeding")
            exit()

# def destroy_everything(check_input=False):
#     """
#     Wrapper to fun through entire inventory in components
#     :param check_input:
#     :return:
#     """
#     if check_input:
#         prompt('destroy', 'everything')
#     comps = import_components()
#     for group in comps['inventory'].key():
#         print(group)


def apply_everything(check_input=False):
    if check_input:
        prompt('apply', 'everything')


def tf_init(comp):
    pass


def tf_apply(comps):
    tf = Terraform(working_dir=str(comp_path))
    tf.plan(no_color=IsFlagged, refresh=False, capture_output=True)

    approve = {"auto-approve": True}
    print(tf.plan())
    # print(tf.apply(**approve))


def tf_destroy(comps):
    pass


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("-c",
                        "--component",
                        default=None,
                        help="The component you want to apply changes to")

    actions = ['apply', 'plan', 'get', 'test']
    parser.add_argument("-a", "--action",
                        choices=actions,
                        default=None,
                        help="The action to take")

    parser.add_argument("-")

    args = parser.parse_args()

    # if args.action == 'init':
    #     tf_init()
    # if args.action == 'apply':
    #     tf_apply()
    # if args.action == 'plan':
    #     tf_apply()
    # if args.action == 'apply' and args.component == 'everything':
    #     apply_everything(check_input=True)


if __name__ == '__main__':
    # main()
    a = Action()
    print(a._get_everything())

    c = Component('stuff')
    print(c.emr)