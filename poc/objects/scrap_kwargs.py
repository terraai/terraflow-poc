

class Foo(object):
    def __init__(self, **kwargs):
        for k in kwargs:
            setattr(self, k, kwargs[k])


class Bar(Foo):
    def __init__(self, baz):
        self.baz = baz
        # super(Bar, self).__init__(**kwargs)

    # def __getattr__(self, item):
    #     return getattr(self.kwargs, item)

foo = Foo(moo = 2, glue = 4)
print(foo.moo)
bar = Bar('weee')
print(bar.moo)



