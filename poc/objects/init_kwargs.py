

class Base(object):

    def __init__(self, **kwargs):
        if type(kwargs) is dict:
            print('wtf')
            for k, v in kwargs[self.__class__.__name__.lower()].items():
                setattr(self, k, v)
        else:
            for k, v in kwargs.items():
                setattr(self, k, v)

    def __getitem__(self, item):
        return self.__dict__.get(item, None)


if __name__ == '__main__':
    import json
    with open('testing-win.json', 'r') as data:
        cfg = json.load(data)

    # print(type(cfg))
    # if type(cfg) is dict:
    #     print('here')

    base = Base(**cfg)
    print(base.stuff)