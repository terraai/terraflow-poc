class Base(object):

    def make_directory(self):
        import os
        os.mkdir('/'.join([self.base_directory]))

    def write_config(self):
        import json
        cfg_name = '/'.join([self.directory, '.'.join([self.name, 'config', 'json'])])
        with open(cfg_name, 'w'):
            json.dump(self.cfg)

