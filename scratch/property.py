class Component(object):
    def __init__(self, name,
                 directory=None):
        self.name = name
        self.directory = directory

    def create_new(self):
        if self.name == 'emr':
            print('making emr')

    @property
    def name(self):
        return self.__name

    @property
    def directory(self):
        return self.__directory

    @directory.setter
    def directory(self, value):
        self.__directory = value

    @name.setter
    def name(self, name):
        if name == 'emr':
            self.__name = name
            self.__directory = 'ferk'
        elif name == 'worker':
            self.__name = name


# class Component(object):
#     def __init__(self, name):
#         self.name = name
#
#     def create_new(self):
#         if self.name == 'emr':
#             print(self.directory)
#
#     @property
#     def directory(self):
#         if self.name == 'emr':
#             return 'ferk'
#         elif self.name == 'worker':
#             return 'ferk2'


c = Component('emr')
c.create_new()
c.directory = 'wham'
print(c.directory)
